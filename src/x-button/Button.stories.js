/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { Button } from './Button';
import readme from './readme.md';

const stories = storiesOf('Button', module);
stories.add(
  'with some emoji and actions',
  () => {
    const children = text('label', ' 😀 😎 👍 💯');
    return (
      <Button
        onClick={action('clicked')}
        onMouseEnter={setCssClass => {
          setCssClass('hovered');
          return action('clicked');
        }}
      >
        <span role="img" aria-label="so cool">
          {children}
        </span>
      </Button>
    );
  },
  {
    notes: { markdown: readme },
  },
);
