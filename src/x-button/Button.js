import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Button = ({
  children,
  primary,
  onClick,
  onMouseEnter = () => {},
  onMouseLeave = () => {},
}) => {
  const [styleClass, setCssClass] = useState('');
  return (
    <button
      type="button"
      className={styleClass}
      onClick={onClick.bind(null, setCssClass)}
      onMouseEnter={() => onMouseEnter(setCssClass)}
      onMouseLeave={() => onMouseLeave(setCssClass)}
      style={{
        display: 'inline-block',
        padding: '0 3em',
        height: 50,
        borderRadius: 50,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: primary ? '#00c4a7' : '#adadad',
        background: primary ? '#00c4a7' : '#fff',
        color: primary ? '#fff' : '#363636',
        font: 'normal 14px/50px sans-serif',
        textRendering: 'optimizeLegibility',
      }}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node,
  primary: PropTypes.bool,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
};

Button.defaultProps = {
  children: '',
  primary: true,
  onClick: () => {},
  onMouseEnter: () => {},
  onMouseLeave: () => {},
};

export { Button };
