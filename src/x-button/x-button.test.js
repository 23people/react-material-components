/* eslint-disable no-console */
import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import * as TestRenderer from 'react-test-renderer';
import { Button } from './Button';

let container;
describe('Test component', () => {
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });
  it('Button changes the class when hovered', () => {
    // assemble
    const component = TestRenderer.create(
      <Button>
        <span role="img" aria-label="so cool">
          😀 😎 👍 💯
        </span>
      </Button>,
    );

    // act
    const tree = component.toJSON();
    // assert
    expect(tree).toMatchSnapshot();
  });

  it('Button trigger click', () => {
    // assemble
    const callback = jest.fn(x => console.log(x));
    act(() => {
      ReactDOM.render(
        <Button onClick={callback} onMouseEnter={setCssClass => setCssClass('hovered')}>
          React
        </Button>,
        container,
      );
    });
    const button = container.querySelector('button');

    // act
    act(() => {
      button.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });
    act(() => {
      const [, props] = Object.values(button);
      props.onMouseEnter();
    });

    // assert
    expect(button.innerHTML).toEqual('React');
    expect(button.className).toEqual('hovered');
    expect(callback.mock.calls.length).toBe(1);
  });
});
