1. Tabla para cargar datos de un Store.

El store es un objeto que define la función load.

`store = { load: function(setRows){...}}`

2. Esta función load tiene como parámetro una función para setear las filas de la Tabla.

La tabla debe además definir las columnas. Las columnas mapean los datos que se van a mostrar por columnas.
Ej.

`const data = [ { id:1, price: 12.5, quantity: 100 }, { id:2, price: 56.2, quantity: 80 }]`

Uso:

`<DataTableColumn index="quantity" label="Cantidad" />`

En el ejemplo anterior se muestra como se mapea el valor de una columna a la fuente de datos mediante la propiedad "index" de la columna.

Para más ejemplo, consultar el story del componente.
