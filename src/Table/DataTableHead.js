/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';

import PropTypes from 'prop-types';
import { TableCell, TableHead, TableRow, TableSortLabel } from '@material-ui/core';

const DataTableColumn = ({ index, type, disablePadding, label, render }) => <Fragment></Fragment>;

DataTableColumn.propTypes = {
  index: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  render: PropTypes.func,
  disablePadding: PropTypes.bool,
  type: PropTypes.string,
};

DataTableColumn.defaultProps = {
  disablePadding: false,
  type: 'string',
  render: identity => identity,
};

const DataTableHeadView = props => {
  const { order, orderBy, onRequestSort, cols } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {cols.map((col = {}) => {
          const { index, type, disablePadding, label } = col;
          return (
            <TableCell
              key={index}
              align={
                type.toLowerCase() === 'numeric'
                  ? 'right'
                  : type.toLowerCase() === 'string'
                  ? 'left'
                  : 'center'
              }
              padding={disablePadding ? 'none' : 'default'}
              sortDirection={orderBy === index ? order : false}
            >
              <TableSortLabel
                active={orderBy === index}
                direction={order}
                onClick={createSortHandler(index)}
              >
                {label}
              </TableSortLabel>
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
  );
};

DataTableHeadView.propTypes = {
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  cols: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const DataTableHead = () => {
  return <Fragment></Fragment>;
};
DataTableHead.displayName = 'DataTableHead';

export { DataTableHead, DataTableHeadView, DataTableColumn };
