import React from 'react';

import PropTypes from 'prop-types';
import { Table, Paper, CircularProgress } from '@material-ui/core';

import { useTableStyles } from './DataTableStyle';
import DataTableToolbar from './DataTableToolbar';
import { DataTableHeadView } from './DataTableHead';
import { DataTableBodyView } from './DataTableBody';

export default function DataTableView(props) {
  const classes = useTableStyles();
  const {
    rows,
    page,
    cols,
    isTableBody,
    setPage,
    setRowsPerPage,
    rowsPerPage,
    order,
    setOrder,
    orderBy,
    setOrderBy,
  } = props;

  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <DataTableToolbar
          rows={rows}
          page={page}
          rowsPerPage={rowsPerPage}
          setPage={setPage}
          setRowsPerPage={setRowsPerPage}
        ></DataTableToolbar>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle" size="medium">
            <DataTableHeadView
              cols={cols}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            ></DataTableHeadView>
            {rows.length > 0 && isTableBody && (
              <DataTableBodyView
                rows={rows}
                cols={cols}
                page={page}
                rowsPerPage={rowsPerPage}
                order={order}
                orderBy={orderBy}
              />
            )}
          </Table>
          {rows.length === 0 && (
            <div className={classes.progress}>
              <CircularProgress />
            </div>
          )}
        </div>
      </Paper>
    </div>
  );
}

DataTableView.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
  page: PropTypes.number,
  cols: PropTypes.arrayOf(PropTypes.object).isRequired,
  isTableBody: PropTypes.bool,
  order: PropTypes.oneOf(['desc', 'asc']),
  orderBy: PropTypes.string,
  rowsPerPage: PropTypes.number,
  setRowsPerPage: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  setOrder: PropTypes.func.isRequired,
  setOrderBy: PropTypes.func.isRequired,
};

DataTableView.defaultProps = {
  page: 0,
  isTableBody: true,
  order: 'asc',
  orderBy: '',
  rowsPerPage: 10,
};
