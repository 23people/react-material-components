/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react';

import PropTypes from 'prop-types';
import { TableBody, TableCell, TableRow } from '@material-ui/core/';

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  return b[orderBy] > a[orderBy] ? 1 : 0;
}

function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

const getSorting = (order, orderBy) =>
  order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);

const DataTableBodyView = ({ cols, rows, page, rowsPerPage, order, orderBy }) => {
  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  const sortedRows = stableSort(rows, getSorting(order, orderBy)).slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage,
  );

  return (
    <TableBody>
      {sortedRows.map(row => {
        return (
          <TableRow hover tabIndex={-1} key={row.id}>
            {cols.map(({ index, type, render = el => el }) => (
              <TableCell
                key={index}
                align={
                  type.toLowerCase() === 'numeric'
                    ? 'right'
                    : type.toLowerCase() === 'string'
                    ? 'left'
                    : 'center'
                }
              >
                {render(row[index])}
              </TableCell>
            ))}
          </TableRow>
        );
      })}
      {emptyRows > 0 && (
        <TableRow style={{ height: 49 * emptyRows }}>
          <TableCell colSpan={6} />
        </TableRow>
      )}
    </TableBody>
  );
};

DataTableBodyView.propTypes = {
  cols: PropTypes.arrayOf(PropTypes.shape({ index: '', render: '' })).isRequired,
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
  order: PropTypes.string,
  orderBy: PropTypes.string,
};

DataTableBodyView.defaultProps = {
  order: 'asc',
  orderBy: '',
};

const DataTableBody = () => <Fragment></Fragment>;
DataTableBody.displayName = 'DataTableBody';

export { DataTableBodyView, DataTableBody };
