/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useReducer } from 'react';

import tableReducer, { ACTIONS } from './DataTableReducer';

const setRows = dispatch => rows => {
  dispatch({
    type: ACTIONS.SET_ROWS,
    data: { rows },
  });
};

const setOrder = dispatch => order => {
  dispatch({
    type: ACTIONS.SET_ORDER,
    data: { order },
  });
};

const setOrderBy = dispatch => orderBy => {
  dispatch({
    type: ACTIONS.SET_ORDER_BY,
    data: { orderBy },
  });
};

const setPage = dispatch => (page, rowsPerPage = 10) => {
  dispatch({
    type: ACTIONS.SET_PAGE,
    data: { page },
  });
};

const setRowsPerPage = dispatch => rowsPerPage => {
  dispatch({
    type: ACTIONS.SET_ROWS_PER_PAGE,
    data: { rowsPerPage },
  });
};

const DataTableContainer = ({
  store = { load: () => {} },
  rowsPerPage = 10,
  order = 'asc',
  orderBy = '',
  page = 0,
  children,
}) => {
  const [tableState, dispatch] = useReducer(tableReducer, {
    rows: [],
    page,
    rowsPerPage,
    order,
    orderBy,
  });

  useEffect(() => {
    store.load(setRows(dispatch));
  }, [store]);

  const [tableHeader] = children.filter(
    child => child.type && child.type.displayName === 'DataTableHead',
  );
  const cols = tableHeader.props.children.map(
    ({ props: { index, type, disablePadding, label, render } }) => ({
      index,
      type,
      disablePadding,
      label,
      render,
    }),
  );

  const [tableBody] = children.filter(
    child => child.type && child.type.displayName === 'DataTableBody',
  );

  return {
    ...tableState,
    cols,
    isTableBody: !!tableBody,
    setRows: setRows(dispatch),
    setOrder: setOrder(dispatch),
    setOrderBy: setOrderBy(dispatch),
    setPage: setPage(dispatch),
    setRowsPerPage: setRowsPerPage(dispatch),
  };
};

export default DataTableContainer;
