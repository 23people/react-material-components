import React from 'react';

import PropTypes from 'prop-types';

import {
  Paper,
  Card,
  CardContent,
  Typography,
  MobileStepper,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';

import { useTableStyles } from './DataTableStyle';
import DataTableToolbar from './DataTableToolbar';

export default function DataTableCard(props) {
  const classes = useTableStyles();
  const { rows, page, cols, setPage, rowsPerPage, setRowsPerPage } = props;

  function handleNext() {
    setPage(page + 1);
  }

  function handleBack() {
    setPage(page - 1);
  }

  const steps = rows.length ? Math.max(Math.round(+rows.length / rowsPerPage), 1) : 1;

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <DataTableToolbar
          rows={rows}
          page={page}
          rowsPerPage={rowsPerPage}
          setPage={setPage}
          setRowsPerPage={setRowsPerPage}
        ></DataTableToolbar>
        {rows.map((row, idx) => (
          <Card className={classes.card} key={row.id || idx}>
            <CardContent>
              {cols.map(({ index, label, render = el => el }) => (
                <div key={index}>
                  <Typography variant="subtitle1">{label}</Typography>
                  <Typography variant="subtitle2" color="textSecondary" gutterBottom>
                    {render(row[index])}
                  </Typography>
                </div>
              ))}
            </CardContent>
          </Card>
        ))}
        {rows.length === 0 && (
          <div className={classes.progress}>
            <CircularProgress />
          </div>
        )}
        <MobileStepper
          variant="dots"
          steps={steps}
          position="static"
          activeStep={page}
          className={classes.root}
          nextButton={
            <Button size="small" onClick={handleNext} disabled={rows.length < rowsPerPage}>
              Next
              <KeyboardArrowRight />
            </Button>
          }
          backButton={
            <Button size="small" onClick={handleBack} disabled={page === 0}>
              <KeyboardArrowLeft />
              Back
            </Button>
          }
        />
      </Paper>
    </div>
  );
}

DataTableCard.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
  page: PropTypes.number,
  cols: PropTypes.arrayOf(PropTypes.object).isRequired,
  setRowsPerPage: PropTypes.func.isRequired,
  rowsPerPage: PropTypes.number,
  setPage: PropTypes.func.isRequired,
};

DataTableCard.defaultProps = {
  page: 0,
  rowsPerPage: 10,
};
