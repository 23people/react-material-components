import React from 'react';

import { useMediaQuery, useTheme } from '@material-ui/core';
import { withStyles, StylesProvider } from '@material-ui/styles';
import DataTableContainer from './DataTableContainer';
import DataTableView from './DataTableView';
import { DataTableBody } from './DataTableBody';
import { DataTableColumn, DataTableHead } from './DataTableHead';
import DataTableCard from './DataTableCard';

const DataTable = withStyles({
  root: {},
})(props => {
  const matches = useMediaQuery(useTheme().breakpoints.up('sm'));

  const withProps = DataTableContainer(props);
  return (
    <StylesProvider injectFirst>
      {matches ? (
        <DataTableView {...withProps} {...props} />
      ) : (
        <DataTableCard {...withProps} {...props} />
      )}
    </StylesProvider>
  );
});

export { DataTable, DataTableBody, DataTableHead, DataTableColumn };
