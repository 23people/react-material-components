/* eslint-disable no-console */
import React from 'react';
import ReactDOM from 'react-dom';
import { render } from '@testing-library/react';
import { renderHook, act } from '@testing-library/react-hooks';

import { DataTableHead, DataTableColumn, DataTableBody } from './index';
import DataTableCard from './DataTableCard';
import DataTableView from './DataTableView';
import DataTableContainer from './DataTableContainer';

let container;
describe('Test component', () => {
  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('render Table', () => {
    // assemble
    // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)

    ReactDOM.render(
      <DataTableView
        cols={[
          { index: 'price', type: 'numeric', label: 'Precio' },
          { index: 'quantity', type: 'numeric', label: 'Cantidad' },
        ]}
        rows={[
          { id: 1, price: 12.5, quantity: 100 },
          { id: 2, price: 56.2, quantity: 80 },
          { id: 3, price: 31.6, quantity: 160 },
        ]}
        setRowsPerPage={() => {}}
        setPage={() => {}}
        setOrder={() => {}}
        setOrderBy={() => {}}
      >
        <DataTableHead>
          <DataTableColumn index="quantity" label="Cantidad" />
          <DataTableColumn index="price" label="Precio" />
        </DataTableHead>
        <DataTableBody />
      </DataTableView>,
      container,
    );

    const { findAllByTestId } = render(
      <DataTableView
        cols={[
          { index: 'price', type: 'numeric', label: 'Precio' },
          { index: 'quantity', type: 'numeric', label: 'Cantidad' },
        ]}
        rows={[
          { id: 1, price: 12.5, quantity: 100 },
          { id: 2, price: 56.2, quantity: 80 },
          { id: 3, price: 31.6, quantity: 160 },
        ]}
        page={0}
        rowsPerPage={10}
        setRowsPerPage={() => {}}
        setPage={() => {}}
        setOrder={() => {}}
        setOrderBy={() => {}}
      >
        <DataTableHead>
          <DataTableColumn index="quantity" label="Cantidad" />
          <DataTableColumn index="price" label="Precio" />
        </DataTableHead>
        <DataTableBody />
      </DataTableView>,
    );

    findAllByTestId('rows')
      .then(([checkElem]) => {
        // const [, props] = Object.values(checkElem);
        // props.onChange({ target: { checked: true } });
        // props.onChange({ target: { checked: false } });
      })
      .catch(e => console.error(new Error(e)));
  });

  it('test DataTableCards', () => {
    ReactDOM.render(
      <DataTableCard
        cols={[
          { index: 'price', type: 'numeric', label: 'Precio' },
          { index: 'quantity', type: 'numeric', label: 'Cantidad' },
        ]}
        rows={[
          { id: 1, price: 12.5, quantity: 100 },
          { id: 2, price: 56.2, quantity: 80 },
          { id: 3, price: 31.6, quantity: 160 },
        ]}
        page={0}
        rowsPerPage={10}
        setRowsPerPage={() => {}}
        setPage={() => {}}
        setOrder={() => {}}
        setOrderBy={() => {}}
      />,
      container,
    );
  });

  it('Test hooks', () => {
    const mockLoadFn = jest.fn();
    const store = {
      load: () => {
        Promise.resolve([]).then(() => mockLoadFn());
      },
    };
    const { result } = renderHook(() =>
      DataTableContainer({
        store,
        children: [
          {
            props: { children: [{ props: { index: 'price', type: 'numeric', label: 'Price' } }] },
            type: { displayName: 'DataTableHead' },
          },
        ],
      }),
    );

    act(() => {
      result.current.setRows([{ id: 1 }]);
      result.current.setOrder('desc');
      result.current.setOrderBy('price');
      result.current.setRowsPerPage(25);
    });

    expect(result.current.rows.length).toBe(1);
    expect(result.current.order).toEqual('desc');
    expect(result.current.orderBy).toEqual('price');
    expect(result.current.rowsPerPage).toBe(25);
    setTimeout(() => {
      expect(mockLoadFn).toBeCalled();
    });
  });
});
