import React from 'react';

import PropTypes from 'prop-types';
import {
  Box,
  Toolbar,
  useMediaQuery,
  useTheme,
  IconButton,
  Tooltip,
  TablePagination,
} from '@material-ui/core';
import { Delete as DeleteIcon } from '@material-ui/icons';

import { useToolbarStyles } from './DataTableStyle';

const DataTableToolbar = ({ rows, page, rowsPerPage, setPage, setRowsPerPage }) => {
  function handleChangePage(event, newPage) {
    setPage(newPage, rowsPerPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0, +event.target.value);
  }
  const matches = useMediaQuery(useTheme().breakpoints.up('sm'));

  const classes = useToolbarStyles();
  return (
    <Toolbar className={classes.root}>
      <Tooltip title="Delete">
        <IconButton aria-label="Delete">
          <DeleteIcon />
        </IconButton>
      </Tooltip>
      {matches && (
        <Box style={{ flex: 2 }}>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={rows.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Box>
      )}
    </Toolbar>
  );
};

DataTableToolbar.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
  page: PropTypes.number,
  rowsPerPage: PropTypes.number,
  setRowsPerPage: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
};

DataTableToolbar.defaultProps = {
  page: 0,
  rowsPerPage: 10,
};

export default DataTableToolbar;
