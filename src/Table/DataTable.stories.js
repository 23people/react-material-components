/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';

import { storiesOf } from '@storybook/react';

import { DataTable, DataTableHead, DataTableColumn, DataTableBody } from './index';

import readme from './readme.md';

const store = {
  load: handlerSetRows => {
    const result = Promise.resolve([
      { id: 1, price: 12.5, quantity: 100 },
      { id: 2, price: 56.2, quantity: 80 },
      { id: 3, price: 31.6, quantity: 160 },
    ]);
    result
      .then(rows => {
        rows && handlerSetRows(rows);
      })
      .catch(e => {
        // eslint-disable-next-line no-console
        console.error(new Error(e));
        handlerSetRows([]);
      });
  },
};

const stories = storiesOf('DataTable', module);
stories.add(
  'Data Table with custom store',
  () => (
    <DataTable store={store}>
      <DataTableHead>
        <DataTableColumn index="quantity" label="Cantidad" />
        <DataTableColumn
          index="price"
          label="Precio"
          render={value => {
            return value < 30 ? (
              <div style={{ color: 'red' }}>{value}</div>
            ) : (
              <div style={{ color: 'green' }}>{value}</div>
            );
          }}
        />
      </DataTableHead>
      <DataTableBody />
    </DataTable>
  ),
  {
    notes: { markdown: readme },
  },
);
