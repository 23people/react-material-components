const ACTIONS = {
  SET_ROWS: 'SET_ROWS',
  SET_PAGE: 'SET_PAGE',
  SET_ORDER: 'SET_ORDER',
  SET_ORDER_BY: 'SET_ORDER_BY',
  SET_ROWS_PER_PAGE: 'SET_ROWS_PER_PAGE',
};
export default function reducer(state, action) {
  switch (action.type) {
    case ACTIONS.SET_ROWS:
      return {
        ...state,
        rows: [...action.data.rows],
      };
    case ACTIONS.SET_ROWS_PER_PAGE:
      return {
        ...state,
        rowsPerPage: action.data.rowsPerPage,
      };
    case ACTIONS.SET_PAGE:
      return {
        ...state,
        page: action.data.page,
      };
    case ACTIONS.SET_ORDER:
      return {
        ...state,
        order: action.data.order,
      };
    case ACTIONS.SET_ORDER_BY:
      return {
        ...state,
        orderBy: action.data.orderBy,
      };
    default:
      return state;
  }
}

export { ACTIONS };
