describe('x-Button E2E', () => {
  const url = '/iframe.html?id=datatable--data-table-with-custom-store';
  it('should have a table with text', () => {
    cy.visit(url);
    cy.get('.MuiTable-root');
  });

  it('should Have columns', () => {
    cy.visit(url);
    cy.get('table > thead > tr > th:nth-child(2) > span').should('have.text', 'Cantidad');
    cy.get('table > thead > tr > th:nth-child(3) > span').should('have.text', 'Precio');
  });
});
