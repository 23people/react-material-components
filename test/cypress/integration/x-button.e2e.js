describe('x-Button E2E', () => {
  const url = '/iframe.html?id=button--with-some-emoji-and-actions';
  it('should have a button with text', () => {
    cy.visit(url);
    cy.get('button > span').should('have.text', ' 😀 😎 👍 💯');
  });

  it('should click and trigger mouse event', () => {
    cy.visit(url);

    cy.get('button')
      .trigger('mouseover')
      .should('have.class', 'hovered');
  });

  it('should click and trigger mouse event', () => {
    cy.visit(url);
    cy.matchImageSnapshot({
      failureThreshold: 0.3,
      failureThresholdType: 'percent',
    });
  });
});
