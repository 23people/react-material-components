'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault(ex) {
  return ex && typeof ex === 'object' && 'default' in ex ? ex['default'] : ex;
}

var React = require('react');
var React__default = _interopDefault(React);
var core = require('@material-ui/core');
var PropTypes = require('prop-types');
var PropTypes__default = _interopDefault(PropTypes);
var icons = require('@material-ui/icons');
require('react-dom');

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true,
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _extends() {
  _extends =
    Object.assign ||
    function(target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

  return _extends.apply(this, arguments);
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly)
      symbols = symbols.filter(function(sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(source, true).forEach(function(key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function(key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArray(iter) {
  if (
    Symbol.iterator in Object(iter) ||
    Object.prototype.toString.call(iter) === '[object Arguments]'
  )
    return Array.from(iter);
}

function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i['return'] != null) _i['return']();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _nonIterableSpread() {
  throw new TypeError('Invalid attempt to spread non-iterable instance');
}

function _nonIterableRest() {
  throw new TypeError('Invalid attempt to destructure non-iterable instance');
}

function _defineProperty$1(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true,
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var defineProperty = _defineProperty$1;

function unwrapExports(x) {
  return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
}

function createCommonjsModule(fn, module) {
  return (module = { exports: {} }), fn(module, module.exports), module.exports;
}

var _extends_1 = createCommonjsModule(function(module) {
  function _extends() {
    module.exports = _extends =
      Object.assign ||
      function(target) {
        for (var i = 1; i < arguments.length; i++) {
          var source = arguments[i];

          for (var key in source) {
            if (Object.prototype.hasOwnProperty.call(source, key)) {
              target[key] = source[key];
            }
          }
        }

        return target;
      };

    return _extends.apply(this, arguments);
  }

  module.exports = _extends;
});

var _typeof_1 = createCommonjsModule(function(module) {
  function _typeof2(obj) {
    if (typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol') {
      _typeof2 = function _typeof2(obj) {
        return typeof obj;
      };
    } else {
      _typeof2 = function _typeof2(obj) {
        return obj &&
          typeof Symbol === 'function' &&
          obj.constructor === Symbol &&
          obj !== Symbol.prototype
          ? 'symbol'
          : typeof obj;
      };
    }
    return _typeof2(obj);
  }

  function _typeof(obj) {
    if (typeof Symbol === 'function' && _typeof2(Symbol.iterator) === 'symbol') {
      module.exports = _typeof = function _typeof(obj) {
        return _typeof2(obj);
      };
    } else {
      module.exports = _typeof = function _typeof(obj) {
        return obj &&
          typeof Symbol === 'function' &&
          obj.constructor === Symbol &&
          obj !== Symbol.prototype
          ? 'symbol'
          : _typeof2(obj);
      };
    }

    return _typeof(obj);
  }

  module.exports = _typeof;
});

var reactIs_production_min = createCommonjsModule(function(module, exports) {
  Object.defineProperty(exports, '__esModule', { value: !0 });
  var b = 'function' === typeof Symbol && Symbol.for,
    c = b ? Symbol.for('react.element') : 60103,
    d = b ? Symbol.for('react.portal') : 60106,
    e = b ? Symbol.for('react.fragment') : 60107,
    f = b ? Symbol.for('react.strict_mode') : 60108,
    g = b ? Symbol.for('react.profiler') : 60114,
    h = b ? Symbol.for('react.provider') : 60109,
    k = b ? Symbol.for('react.context') : 60110,
    l = b ? Symbol.for('react.async_mode') : 60111,
    m = b ? Symbol.for('react.concurrent_mode') : 60111,
    n = b ? Symbol.for('react.forward_ref') : 60112,
    p = b ? Symbol.for('react.suspense') : 60113,
    q = b ? Symbol.for('react.memo') : 60115,
    r = b ? Symbol.for('react.lazy') : 60116;
  function t(a) {
    if ('object' === typeof a && null !== a) {
      var u = a.$$typeof;
      switch (u) {
        case c:
          switch (((a = a.type), a)) {
            case l:
            case m:
            case e:
            case g:
            case f:
            case p:
              return a;
            default:
              switch (((a = a && a.$$typeof), a)) {
                case k:
                case n:
                case h:
                  return a;
                default:
                  return u;
              }
          }
        case r:
        case q:
        case d:
          return u;
      }
    }
  }
  function v(a) {
    return t(a) === m;
  }
  exports.typeOf = t;
  exports.AsyncMode = l;
  exports.ConcurrentMode = m;
  exports.ContextConsumer = k;
  exports.ContextProvider = h;
  exports.Element = c;
  exports.ForwardRef = n;
  exports.Fragment = e;
  exports.Lazy = r;
  exports.Memo = q;
  exports.Portal = d;
  exports.Profiler = g;
  exports.StrictMode = f;
  exports.Suspense = p;
  exports.isValidElementType = function(a) {
    return (
      'string' === typeof a ||
      'function' === typeof a ||
      a === e ||
      a === m ||
      a === g ||
      a === f ||
      a === p ||
      ('object' === typeof a &&
        null !== a &&
        (a.$$typeof === r ||
          a.$$typeof === q ||
          a.$$typeof === h ||
          a.$$typeof === k ||
          a.$$typeof === n))
    );
  };
  exports.isAsyncMode = function(a) {
    return v(a) || t(a) === l;
  };
  exports.isConcurrentMode = v;
  exports.isContextConsumer = function(a) {
    return t(a) === k;
  };
  exports.isContextProvider = function(a) {
    return t(a) === h;
  };
  exports.isElement = function(a) {
    return 'object' === typeof a && null !== a && a.$$typeof === c;
  };
  exports.isForwardRef = function(a) {
    return t(a) === n;
  };
  exports.isFragment = function(a) {
    return t(a) === e;
  };
  exports.isLazy = function(a) {
    return t(a) === r;
  };
  exports.isMemo = function(a) {
    return t(a) === q;
  };
  exports.isPortal = function(a) {
    return t(a) === d;
  };
  exports.isProfiler = function(a) {
    return t(a) === g;
  };
  exports.isStrictMode = function(a) {
    return t(a) === f;
  };
  exports.isSuspense = function(a) {
    return t(a) === p;
  };
});

unwrapExports(reactIs_production_min);
var reactIs_production_min_1 = reactIs_production_min.typeOf;
var reactIs_production_min_2 = reactIs_production_min.AsyncMode;
var reactIs_production_min_3 = reactIs_production_min.ConcurrentMode;
var reactIs_production_min_4 = reactIs_production_min.ContextConsumer;
var reactIs_production_min_5 = reactIs_production_min.ContextProvider;
var reactIs_production_min_6 = reactIs_production_min.Element;
var reactIs_production_min_7 = reactIs_production_min.ForwardRef;
var reactIs_production_min_8 = reactIs_production_min.Fragment;
var reactIs_production_min_9 = reactIs_production_min.Lazy;
var reactIs_production_min_10 = reactIs_production_min.Memo;
var reactIs_production_min_11 = reactIs_production_min.Portal;
var reactIs_production_min_12 = reactIs_production_min.Profiler;
var reactIs_production_min_13 = reactIs_production_min.StrictMode;
var reactIs_production_min_14 = reactIs_production_min.Suspense;
var reactIs_production_min_15 = reactIs_production_min.isValidElementType;
var reactIs_production_min_16 = reactIs_production_min.isAsyncMode;
var reactIs_production_min_17 = reactIs_production_min.isConcurrentMode;
var reactIs_production_min_18 = reactIs_production_min.isContextConsumer;
var reactIs_production_min_19 = reactIs_production_min.isContextProvider;
var reactIs_production_min_20 = reactIs_production_min.isElement;
var reactIs_production_min_21 = reactIs_production_min.isForwardRef;
var reactIs_production_min_22 = reactIs_production_min.isFragment;
var reactIs_production_min_23 = reactIs_production_min.isLazy;
var reactIs_production_min_24 = reactIs_production_min.isMemo;
var reactIs_production_min_25 = reactIs_production_min.isPortal;
var reactIs_production_min_26 = reactIs_production_min.isProfiler;
var reactIs_production_min_27 = reactIs_production_min.isStrictMode;
var reactIs_production_min_28 = reactIs_production_min.isSuspense;

var reactIs = createCommonjsModule(function(module) {
  {
    module.exports = reactIs_production_min;
  }
});
var reactIs_1 = reactIs.isElement;
var reactIs_2 = reactIs.isValidElementType;
var reactIs_3 = reactIs.ForwardRef;

var hasSymbol = typeof Symbol === 'function';
var nested = hasSymbol ? Symbol.for('mui.nested') : '__THEME_NESTED__';

/**
 * This is the list of the style rule name we use as drop in replacement for the built-in
 * pseudo classes (:checked, :disabled, :focused, etc.).
 *
 * Why do they exist in the first place?
 * These classes are used at a specificity of 2.
 * It allows them to override previously definied styles as well as
 * being untouched by simple user overrides.
 */

var pseudoClasses = [
  'checked',
  'disabled',
  'error',
  'focused',
  'focusVisible',
  'required',
  'expanded',
  'selected',
]; // Returns a function which generates unique class names based on counters.
// When new generator function is created, rule counter is reset.
// We need to reset the rule counter for SSR for each request.
//
// It's inspired by
// https://github.com/cssinjs/jss/blob/4e6a05dd3f7b6572fdd3ab216861d9e446c20331/src/utils/createGenerateClassName.js

function createGenerateClassName() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _options$disableGloba = options.disableGlobal,
    disableGlobal = _options$disableGloba === void 0 ? false : _options$disableGloba,
    _options$productionPr = options.productionPrefix,
    productionPrefix = _options$productionPr === void 0 ? 'jss' : _options$productionPr,
    _options$seed = options.seed,
    seed = _options$seed === void 0 ? '' : _options$seed;
  var seedPrefix = seed === '' ? '' : ''.concat(seed, '-');
  var ruleCounter = 0;
  return function(rule, styleSheet) {
    ruleCounter += 1;
    var name = styleSheet.options.name; // Is a global static MUI style?

    if (name && name.indexOf('Mui') === 0 && !styleSheet.options.link && !disableGlobal) {
      // We can use a shorthand class name, we never use the keys to style the components.
      if (pseudoClasses.indexOf(rule.key) !== -1) {
        return 'Mui-'.concat(rule.key);
      }

      var prefix = ''
        .concat(seedPrefix)
        .concat(name, '-')
        .concat(rule.key);

      if (!styleSheet.options.theme[nested] || seed !== '') {
        return prefix;
      }

      return ''.concat(prefix, '-').concat(ruleCounter);
    }

    {
      return ''
        .concat(seedPrefix)
        .concat(productionPrefix)
        .concat(ruleCounter);
    }

    var suffix = ''.concat(rule.key, '-').concat(ruleCounter); // Help with debuggability.

    if (styleSheet.options.classNamePrefix) {
      return ''
        .concat(seedPrefix)
        .concat(styleSheet.options.classNamePrefix, '-')
        .concat(suffix);
    }

    return ''.concat(seedPrefix).concat(suffix);
  };
}

/* eslint-disable no-restricted-syntax */
function getThemeProps(params) {
  var theme = params.theme,
    name = params.name,
    props = params.props;

  if (!theme || !theme.props || !theme.props[name]) {
    return props;
  } // Resolve default props, code borrow from React source.
  // https://github.com/facebook/react/blob/15a8f031838a553e41c0b66eb1bcf1da8448104d/packages/react/src/ReactElement.js#L221

  var defaultProps = theme.props[name];
  var propName;

  for (propName in defaultProps) {
    if (props[propName] === undefined) {
      props[propName] = defaultProps[propName];
    }
  }

  return props;
}

function _extends$1() {
  _extends$1 =
    Object.assign ||
    function(target) {
      for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i];

        for (var key in source) {
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }

      return target;
    };

  return _extends$1.apply(this, arguments);
}

var _typeof =
  typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol'
    ? function(obj) {
        return typeof obj;
      }
    : function(obj) {
        return obj &&
          typeof Symbol === 'function' &&
          obj.constructor === Symbol &&
          obj !== Symbol.prototype
          ? 'symbol'
          : typeof obj;
      };

var isBrowser =
  (typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' &&
  (typeof document === 'undefined' ? 'undefined' : _typeof(document)) === 'object' &&
  document.nodeType === 9;

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ('value' in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;
  subClass.__proto__ = superClass;
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

var plainObjectConstrurctor = {}.constructor;
function cloneStyle(style) {
  if (style == null || typeof style !== 'object') return style;
  if (Array.isArray(style)) return style.map(cloneStyle);
  if (style.constructor !== plainObjectConstrurctor) return style;
  var newStyle = {};

  for (var name in style) {
    newStyle[name] = cloneStyle(style[name]);
  }

  return newStyle;
}

/**
 * Create a rule instance.
 */

function createRule(name, decl, options) {
  if (name === void 0) {
    name = 'unnamed';
  }

  var jss = options.jss;
  var declCopy = cloneStyle(decl);
  var rule = jss.plugins.onCreateRule(name, declCopy, options);
  if (rule) return rule; // It is an at-rule and it has no instance.

  if (name[0] === '@');

  return null;
}

var join = function join(value, by) {
  var result = '';

  for (var i = 0; i < value.length; i++) {
    // Remove !important from the value, it will be readded later.
    if (value[i] === '!important') break;
    if (result) result += by;
    result += value[i];
  }

  return result;
};
/**
 * Converts array values to string.
 *
 * `margin: [['5px', '10px']]` > `margin: 5px 10px;`
 * `border: ['1px', '2px']` > `border: 1px, 2px;`
 * `margin: [['5px', '10px'], '!important']` > `margin: 5px 10px !important;`
 * `color: ['red', !important]` > `color: red !important;`
 */

function toCssValue(value, ignoreImportant) {
  if (ignoreImportant === void 0) {
    ignoreImportant = false;
  }

  if (!Array.isArray(value)) return value;
  var cssValue = ''; // Support space separated values via `[['5px', '10px']]`.

  if (Array.isArray(value[0])) {
    for (var i = 0; i < value.length; i++) {
      if (value[i] === '!important') break;
      if (cssValue) cssValue += ', ';
      cssValue += join(value[i], ' ');
    }
  } else cssValue = join(value, ', '); // Add !important, because it was ignored.

  if (!ignoreImportant && value[value.length - 1] === '!important') {
    cssValue += ' !important';
  }

  return cssValue;
}

/**
 * Indent a string.
 * http://jsperf.com/array-join-vs-for
 */
function indentStr(str, indent) {
  var result = '';

  for (var index = 0; index < indent; index++) {
    result += '  ';
  }

  return result + str;
}
/**
 * Converts a Rule to CSS string.
 */

function toCss(selector, style, options) {
  if (options === void 0) {
    options = {};
  }

  var result = '';
  if (!style) return result;
  var _options = options,
    _options$indent = _options.indent,
    indent = _options$indent === void 0 ? 0 : _options$indent;
  var fallbacks = style.fallbacks;
  if (selector) indent++; // Apply fallbacks first.

  if (fallbacks) {
    // Array syntax {fallbacks: [{prop: value}]}
    if (Array.isArray(fallbacks)) {
      for (var index = 0; index < fallbacks.length; index++) {
        var fallback = fallbacks[index];

        for (var prop in fallback) {
          var value = fallback[prop];

          if (value != null) {
            if (result) result += '\n';
            result += '' + indentStr(prop + ': ' + toCssValue(value) + ';', indent);
          }
        }
      }
    } else {
      // Object syntax {fallbacks: {prop: value}}
      for (var _prop in fallbacks) {
        var _value = fallbacks[_prop];

        if (_value != null) {
          if (result) result += '\n';
          result += '' + indentStr(_prop + ': ' + toCssValue(_value) + ';', indent);
        }
      }
    }
  }

  for (var _prop2 in style) {
    var _value2 = style[_prop2];

    if (_value2 != null && _prop2 !== 'fallbacks') {
      if (result) result += '\n';
      result += '' + indentStr(_prop2 + ': ' + toCssValue(_value2) + ';', indent);
    }
  } // Allow empty style in this case, because properties will be added dynamically.

  if (!result && !options.allowEmpty) return result; // When rule is being stringified before selector was defined.

  if (!selector) return result;
  indent--;
  if (result) result = '\n' + result + '\n';
  return indentStr(selector + ' {' + result, indent) + indentStr('}', indent);
}

var escapeRegex = /([[\].#*$><+~=|^:(),"'`\s])/g;
var nativeEscape = typeof CSS !== 'undefined' && CSS.escape;
var escape = function(str) {
  return nativeEscape ? nativeEscape(str) : str.replace(escapeRegex, '\\$1');
};

var BaseStyleRule =
  /*#__PURE__*/
  (function() {
    function BaseStyleRule(key, style, options) {
      this.type = 'style';
      this.key = void 0;
      this.isProcessed = false;
      this.style = void 0;
      this.renderer = void 0;
      this.renderable = void 0;
      this.options = void 0;
      var sheet = options.sheet,
        Renderer = options.Renderer;
      this.key = key;
      this.options = options;
      this.style = style;
      if (sheet) this.renderer = sheet.renderer;
      else if (Renderer) this.renderer = new Renderer();
    }
    /**
     * Get or set a style property.
     */

    var _proto = BaseStyleRule.prototype;

    _proto.prop = function prop(name, value, options) {
      // It's a getter.
      if (value === undefined) return this.style[name]; // Don't do anything if the value has not changed.

      var force = options ? options.force : false;
      if (!force && this.style[name] === value) return this;
      var newValue = value;

      if (!options || options.process !== false) {
        newValue = this.options.jss.plugins.onChangeValue(value, name, this);
      }

      var isEmpty = newValue == null || newValue === false;
      var isDefined = name in this.style; // Value is empty and wasn't defined before.

      if (isEmpty && !isDefined && !force) return this; // We are going to remove this value.

      var remove = isEmpty && isDefined;
      if (remove) delete this.style[name];
      else this.style[name] = newValue; // Renderable is defined if StyleSheet option `link` is true.

      if (this.renderable && this.renderer) {
        if (remove) this.renderer.removeProperty(this.renderable, name);
        else this.renderer.setProperty(this.renderable, name, newValue);
        return this;
      }

      var sheet = this.options.sheet;

      if (sheet && sheet.attached);

      return this;
    };

    return BaseStyleRule;
  })();
var StyleRule =
  /*#__PURE__*/
  (function(_BaseStyleRule) {
    _inheritsLoose(StyleRule, _BaseStyleRule);

    function StyleRule(key, style, options) {
      var _this;

      _this = _BaseStyleRule.call(this, key, style, options) || this;
      _this.selectorText = void 0;
      _this.id = void 0;
      _this.renderable = void 0;
      var selector = options.selector,
        scoped = options.scoped,
        sheet = options.sheet,
        generateId = options.generateId;

      if (selector) {
        _this.selectorText = selector;
      } else if (scoped !== false) {
        _this.id = generateId(_assertThisInitialized(_assertThisInitialized(_this)), sheet);
        _this.selectorText = '.' + escape(_this.id);
      }

      return _this;
    }
    /**
     * Set selector string.
     * Attention: use this with caution. Most browsers didn't implement
     * selectorText setter, so this may result in rerendering of entire Style Sheet.
     */

    var _proto2 = StyleRule.prototype;

    /**
     * Apply rule to an element inline.
     */
    _proto2.applyTo = function applyTo(renderable) {
      var renderer = this.renderer;

      if (renderer) {
        var json = this.toJSON();

        for (var prop in json) {
          renderer.setProperty(renderable, prop, json[prop]);
        }
      }

      return this;
    };
    /**
     * Returns JSON representation of the rule.
     * Fallbacks are not supported.
     * Useful for inline styles.
     */

    _proto2.toJSON = function toJSON() {
      var json = {};

      for (var prop in this.style) {
        var value = this.style[prop];
        if (typeof value !== 'object') json[prop] = value;
        else if (Array.isArray(value)) json[prop] = toCssValue(value);
      }

      return json;
    };
    /**
     * Generates a CSS string.
     */

    _proto2.toString = function toString(options) {
      var sheet = this.options.sheet;
      var link = sheet ? sheet.options.link : false;
      var opts = link
        ? _extends$1({}, options, {
            allowEmpty: true,
          })
        : options;
      return toCss(this.selectorText, this.style, opts);
    };

    _createClass(StyleRule, [
      {
        key: 'selector',
        set: function set(selector) {
          if (selector === this.selectorText) return;
          this.selectorText = selector;
          var renderer = this.renderer,
            renderable = this.renderable;
          if (!renderable || !renderer) return;
          var hasChanged = renderer.setSelector(renderable, selector); // If selector setter is not implemented, rerender the rule.

          if (!hasChanged) {
            renderer.replaceRule(renderable, this);
          }
        },
        /**
         * Get selector string.
         */
        get: function get() {
          return this.selectorText;
        },
      },
    ]);

    return StyleRule;
  })(BaseStyleRule);
var pluginStyleRule = {
  onCreateRule: function onCreateRule(name, style, options) {
    if (name[0] === '@' || (options.parent && options.parent.type === 'keyframes')) {
      return null;
    }

    return new StyleRule(name, style, options);
  },
};

var defaultToStringOptions = {
  indent: 1,
  children: true,
};
var atRegExp = /@([\w-]+)/;
/**
 * Conditional rule for @media, @supports
 */

var ConditionalRule =
  /*#__PURE__*/
  (function() {
    function ConditionalRule(key, styles, options) {
      this.type = 'conditional';
      this.at = void 0;
      this.key = void 0;
      this.rules = void 0;
      this.options = void 0;
      this.isProcessed = false;
      this.renderable = void 0;
      this.key = key;
      var atMatch = key.match(atRegExp);
      this.at = atMatch ? atMatch[1] : 'unknown';
      this.options = options;
      this.rules = new RuleList(
        _extends$1({}, options, {
          parent: this,
        }),
      );

      for (var name in styles) {
        this.rules.add(name, styles[name]);
      }

      this.rules.process();
    }
    /**
     * Get a rule.
     */

    var _proto = ConditionalRule.prototype;

    _proto.getRule = function getRule(name) {
      return this.rules.get(name);
    };
    /**
     * Get index of a rule.
     */

    _proto.indexOf = function indexOf(rule) {
      return this.rules.indexOf(rule);
    };
    /**
     * Create and register rule, run plugins.
     */

    _proto.addRule = function addRule(name, style, options) {
      var rule = this.rules.add(name, style, options);
      if (!rule) return null;
      this.options.jss.plugins.onProcessRule(rule);
      return rule;
    };
    /**
     * Generates a CSS string.
     */

    _proto.toString = function toString(options) {
      if (options === void 0) {
        options = defaultToStringOptions;
      }

      if (options.children === false) {
        return this.key + ' {}';
      }

      var children = this.rules.toString(options);
      return children ? this.key + ' {\n' + children + '\n}' : '';
    };

    return ConditionalRule;
  })();
var keyRegExp = /@media|@supports\s+/;
var pluginConditionalRule = {
  onCreateRule: function onCreateRule(key, styles, options) {
    return keyRegExp.test(key) ? new ConditionalRule(key, styles, options) : null;
  },
};

var defaultToStringOptions$1 = {
  indent: 1,
  children: true,
};
var nameRegExp = /@keyframes\s+([\w-]+)/;
/**
 * Rule for @keyframes
 */

var KeyframesRule =
  /*#__PURE__*/
  (function() {
    function KeyframesRule(key, frames, options) {
      this.type = 'keyframes';
      this.at = '@keyframes';
      this.key = void 0;
      this.name = void 0;
      this.id = void 0;
      this.rules = void 0;
      this.options = void 0;
      this.isProcessed = false;
      this.renderable = void 0;
      var nameMatch = key.match(nameRegExp);

      if (nameMatch && nameMatch[1]) {
        this.name = nameMatch[1];
      } else {
        this.name = 'noname';
      }

      this.key = this.type + '-' + this.name;
      this.options = options;
      var scoped = options.scoped,
        sheet = options.sheet,
        generateId = options.generateId;
      this.id = scoped === false ? this.name : escape(generateId(this, sheet));
      this.rules = new RuleList(
        _extends$1({}, options, {
          parent: this,
        }),
      );

      for (var name in frames) {
        this.rules.add(
          name,
          frames[name],
          _extends$1({}, options, {
            parent: this,
          }),
        );
      }

      this.rules.process();
    }
    /**
     * Generates a CSS string.
     */

    var _proto = KeyframesRule.prototype;

    _proto.toString = function toString(options) {
      if (options === void 0) {
        options = defaultToStringOptions$1;
      }

      if (options.children === false) {
        return this.at + ' ' + this.id + ' {}';
      }

      var children = this.rules.toString(options);
      if (children) children = '\n' + children + '\n';
      return this.at + ' ' + this.id + ' {' + children + '}';
    };

    return KeyframesRule;
  })();
var keyRegExp$1 = /@keyframes\s+/;
var refRegExp = /\$([\w-]+)/g;

var findReferencedKeyframe = function findReferencedKeyframe(val, keyframes) {
  if (typeof val === 'string') {
    return val.replace(refRegExp, function(match, name) {
      if (name in keyframes) {
        return keyframes[name];
      }
      return match;
    });
  }

  return val;
};
/**
 * Replace the reference for a animation name.
 */

var replaceRef = function replaceRef(style, prop, keyframes) {
  var value = style[prop];
  var refKeyframe = findReferencedKeyframe(value, keyframes);

  if (refKeyframe !== value) {
    style[prop] = refKeyframe;
  }
};

var plugin = {
  onCreateRule: function onCreateRule(key, frames, options) {
    return typeof key === 'string' && keyRegExp$1.test(key)
      ? new KeyframesRule(key, frames, options)
      : null;
  },
  // Animation name ref replacer.
  onProcessStyle: function onProcessStyle(style, rule, sheet) {
    if (rule.type !== 'style' || !sheet) return style;
    if ('animation-name' in style) replaceRef(style, 'animation-name', sheet.keyframes);
    if ('animation' in style) replaceRef(style, 'animation', sheet.keyframes);
    return style;
  },
  onChangeValue: function onChangeValue(val, prop, rule) {
    var sheet = rule.options.sheet;

    if (!sheet) {
      return val;
    }

    switch (prop) {
      case 'animation':
        return findReferencedKeyframe(val, sheet.keyframes);

      case 'animation-name':
        return findReferencedKeyframe(val, sheet.keyframes);

      default:
        return val;
    }
  },
};

var KeyframeRule =
  /*#__PURE__*/
  (function(_BaseStyleRule) {
    _inheritsLoose(KeyframeRule, _BaseStyleRule);

    function KeyframeRule() {
      var _this;

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      _this = _BaseStyleRule.call.apply(_BaseStyleRule, [this].concat(args)) || this;
      _this.renderable = void 0;
      return _this;
    }

    var _proto = KeyframeRule.prototype;

    /**
     * Generates a CSS string.
     */
    _proto.toString = function toString(options) {
      var sheet = this.options.sheet;
      var link = sheet ? sheet.options.link : false;
      var opts = link
        ? _extends$1({}, options, {
            allowEmpty: true,
          })
        : options;
      return toCss(this.key, this.style, opts);
    };

    return KeyframeRule;
  })(BaseStyleRule);
var pluginKeyframeRule = {
  onCreateRule: function onCreateRule(key, style, options) {
    if (options.parent && options.parent.type === 'keyframes') {
      return new KeyframeRule(key, style, options);
    }

    return null;
  },
};

var FontFaceRule =
  /*#__PURE__*/
  (function() {
    function FontFaceRule(key, style, options) {
      this.type = 'font-face';
      this.at = '@font-face';
      this.key = void 0;
      this.style = void 0;
      this.options = void 0;
      this.isProcessed = false;
      this.renderable = void 0;
      this.key = key;
      this.style = style;
      this.options = options;
    }
    /**
     * Generates a CSS string.
     */

    var _proto = FontFaceRule.prototype;

    _proto.toString = function toString(options) {
      if (Array.isArray(this.style)) {
        var str = '';

        for (var index = 0; index < this.style.length; index++) {
          str += toCss(this.key, this.style[index]);
          if (this.style[index + 1]) str += '\n';
        }

        return str;
      }

      return toCss(this.key, this.style, options);
    };

    return FontFaceRule;
  })();
var pluginFontFaceRule = {
  onCreateRule: function onCreateRule(key, style, options) {
    return key === '@font-face' ? new FontFaceRule(key, style, options) : null;
  },
};

var ViewportRule =
  /*#__PURE__*/
  (function() {
    function ViewportRule(key, style, options) {
      this.type = 'viewport';
      this.at = '@viewport';
      this.key = void 0;
      this.style = void 0;
      this.options = void 0;
      this.isProcessed = false;
      this.renderable = void 0;
      this.key = key;
      this.style = style;
      this.options = options;
    }
    /**
     * Generates a CSS string.
     */

    var _proto = ViewportRule.prototype;

    _proto.toString = function toString(options) {
      return toCss(this.key, this.style, options);
    };

    return ViewportRule;
  })();
var pluginViewportRule = {
  onCreateRule: function onCreateRule(key, style, options) {
    return key === '@viewport' || key === '@-ms-viewport'
      ? new ViewportRule(key, style, options)
      : null;
  },
};

var SimpleRule =
  /*#__PURE__*/
  (function() {
    function SimpleRule(key, value, options) {
      this.type = 'simple';
      this.key = void 0;
      this.value = void 0;
      this.options = void 0;
      this.isProcessed = false;
      this.renderable = void 0;
      this.key = key;
      this.value = value;
      this.options = options;
    }
    /**
     * Generates a CSS string.
     */
    // eslint-disable-next-line no-unused-vars

    var _proto = SimpleRule.prototype;

    _proto.toString = function toString(options) {
      if (Array.isArray(this.value)) {
        var str = '';

        for (var index = 0; index < this.value.length; index++) {
          str += this.key + ' ' + this.value[index] + ';';
          if (this.value[index + 1]) str += '\n';
        }

        return str;
      }

      return this.key + ' ' + this.value + ';';
    };

    return SimpleRule;
  })();
var keysMap = {
  '@charset': true,
  '@import': true,
  '@namespace': true,
};
var pluginSimpleRule = {
  onCreateRule: function onCreateRule(key, value, options) {
    return key in keysMap ? new SimpleRule(key, value, options) : null;
  },
};

var plugins = [
  pluginStyleRule,
  pluginConditionalRule,
  plugin,
  pluginKeyframeRule,
  pluginFontFaceRule,
  pluginViewportRule,
  pluginSimpleRule,
];

var defaultUpdateOptions = {
  process: true,
};
var forceUpdateOptions = {
  force: true,
  process: true,
  /**
   * Contains rules objects and allows adding/removing etc.
   * Is used for e.g. by `StyleSheet` or `ConditionalRule`.
   */
};

var RuleList =
  /*#__PURE__*/
  (function() {
    // Rules registry for access by .get() method.
    // It contains the same rule registered by name and by selector.
    // Original styles object.
    // Used to ensure correct rules order.
    function RuleList(options) {
      this.map = {};
      this.raw = {};
      this.index = [];
      this.options = void 0;
      this.classes = void 0;
      this.keyframes = void 0;
      this.options = options;
      this.classes = options.classes;
      this.keyframes = options.keyframes;
    }
    /**
     * Create and register rule.
     *
     * Will not render after Style Sheet was rendered the first time.
     */

    var _proto = RuleList.prototype;

    _proto.add = function add(key, decl, ruleOptions) {
      var _this$options = this.options,
        parent = _this$options.parent,
        sheet = _this$options.sheet,
        jss = _this$options.jss,
        Renderer = _this$options.Renderer,
        generateId = _this$options.generateId,
        scoped = _this$options.scoped;

      var options = _extends$1(
        {
          classes: this.classes,
          parent: parent,
          sheet: sheet,
          jss: jss,
          Renderer: Renderer,
          generateId: generateId,
          scoped: scoped,
        },
        ruleOptions,
      ); // We need to save the original decl before creating the rule
      // because cache plugin needs to use it as a key to return a cached rule.

      this.raw[key] = decl;

      if (key in this.classes) {
        // For e.g. rules inside of @media container
        options.selector = '.' + escape(this.classes[key]);
      }

      var rule = createRule(key, decl, options);
      if (!rule) return null;
      this.register(rule);
      var index = options.index === undefined ? this.index.length : options.index;
      this.index.splice(index, 0, rule);
      return rule;
    };
    /**
     * Get a rule.
     */

    _proto.get = function get(name) {
      return this.map[name];
    };
    /**
     * Delete a rule.
     */

    _proto.remove = function remove(rule) {
      this.unregister(rule);
      delete this.raw[rule.key];
      this.index.splice(this.indexOf(rule), 1);
    };
    /**
     * Get index of a rule.
     */

    _proto.indexOf = function indexOf(rule) {
      return this.index.indexOf(rule);
    };
    /**
     * Run `onProcessRule()` plugins on every rule.
     */

    _proto.process = function process() {
      var plugins$$1 = this.options.jss.plugins; // We need to clone array because if we modify the index somewhere else during a loop
      // we end up with very hard-to-track-down side effects.

      this.index.slice(0).forEach(plugins$$1.onProcessRule, plugins$$1);
    };
    /**
     * Register a rule in `.map` and `.classes` maps.
     */

    _proto.register = function register(rule) {
      this.map[rule.key] = rule;

      if (rule instanceof StyleRule) {
        this.map[rule.selector] = rule;
        if (rule.id) this.classes[rule.key] = rule.id;
      } else if (rule instanceof KeyframesRule && this.keyframes) {
        this.keyframes[rule.name] = rule.id;
      }
    };
    /**
     * Unregister a rule.
     */

    _proto.unregister = function unregister(rule) {
      delete this.map[rule.key];

      if (rule instanceof StyleRule) {
        delete this.map[rule.selector];
        delete this.classes[rule.key];
      } else if (rule instanceof KeyframesRule) {
        delete this.keyframes[rule.name];
      }
    };
    /**
     * Update the function values with a new data.
     */

    _proto.update = function update() {
      var name;
      var data;
      var options;

      if (typeof (arguments.length <= 0 ? undefined : arguments[0]) === 'string') {
        name = arguments.length <= 0 ? undefined : arguments[0]; // $FlowFixMe

        data = arguments.length <= 1 ? undefined : arguments[1]; // $FlowFixMe

        options = arguments.length <= 2 ? undefined : arguments[2];
      } else {
        data = arguments.length <= 0 ? undefined : arguments[0]; // $FlowFixMe

        options = arguments.length <= 1 ? undefined : arguments[1];
        name = null;
      }

      if (name) {
        this.onUpdate(data, this.get(name), options);
      } else {
        for (var index = 0; index < this.index.length; index++) {
          this.onUpdate(data, this.index[index], options);
        }
      }
    };
    /**
     * Execute plugins, update rule props.
     */

    _proto.onUpdate = function onUpdate(data, rule, options) {
      if (options === void 0) {
        options = defaultUpdateOptions;
      }

      var _this$options2 = this.options,
        plugins$$1 = _this$options2.jss.plugins,
        sheet = _this$options2.sheet; // It is a rules container like for e.g. ConditionalRule.

      if (rule.rules instanceof RuleList) {
        rule.rules.update(data, options);
        return;
      }

      var styleRule = rule;
      var style = styleRule.style;
      plugins$$1.onUpdate(data, rule, sheet, options); // We rely on a new `style` ref in case it was mutated during onUpdate hook.

      if (options.process && style && style !== styleRule.style) {
        // We need to run the plugins in case new `style` relies on syntax plugins.
        plugins$$1.onProcessStyle(styleRule.style, styleRule, sheet); // Update and add props.

        for (var prop in styleRule.style) {
          var nextValue = styleRule.style[prop];
          var prevValue = style[prop]; // We need to use `force: true` because `rule.style` has been updated during onUpdate hook, so `rule.prop()` will not update the CSSOM rule.
          // We do this comparison to avoid unneeded `rule.prop()` calls, since we have the old `style` object here.

          if (nextValue !== prevValue) {
            styleRule.prop(prop, nextValue, forceUpdateOptions);
          }
        } // Remove props.

        for (var _prop in style) {
          var _nextValue = styleRule.style[_prop];
          var _prevValue = style[_prop]; // We need to use `force: true` because `rule.style` has been updated during onUpdate hook, so `rule.prop()` will not update the CSSOM rule.
          // We do this comparison to avoid unneeded `rule.prop()` calls, since we have the old `style` object here.

          if (_nextValue == null && _nextValue !== _prevValue) {
            styleRule.prop(_prop, null, forceUpdateOptions);
          }
        }
      }
    };
    /**
     * Convert rules to a CSS string.
     */

    _proto.toString = function toString(options) {
      var str = '';
      var sheet = this.options.sheet;
      var link = sheet ? sheet.options.link : false;

      for (var index = 0; index < this.index.length; index++) {
        var rule = this.index[index];
        var css = rule.toString(options); // No need to render an empty rule.

        if (!css && !link) continue;
        if (str) str += '\n';
        str += css;
      }

      return str;
    };

    return RuleList;
  })();

var StyleSheet =
  /*#__PURE__*/
  (function() {
    function StyleSheet(styles, options) {
      this.options = void 0;
      this.deployed = void 0;
      this.attached = void 0;
      this.rules = void 0;
      this.renderer = void 0;
      this.classes = void 0;
      this.keyframes = void 0;
      this.queue = void 0;
      this.attached = false;
      this.deployed = false;
      this.classes = {};
      this.keyframes = {};
      this.options = _extends$1({}, options, {
        sheet: this,
        parent: this,
        classes: this.classes,
        keyframes: this.keyframes,
      });

      if (options.Renderer) {
        this.renderer = new options.Renderer(this);
      }

      this.rules = new RuleList(this.options);

      for (var name in styles) {
        this.rules.add(name, styles[name]);
      }

      this.rules.process();
    }
    /**
     * Attach renderable to the render tree.
     */

    var _proto = StyleSheet.prototype;

    _proto.attach = function attach() {
      if (this.attached) return this;
      if (this.renderer) this.renderer.attach();
      this.attached = true; // Order is important, because we can't use insertRule API if style element is not attached.

      if (!this.deployed) this.deploy();
      return this;
    };
    /**
     * Remove renderable from render tree.
     */

    _proto.detach = function detach() {
      if (!this.attached) return this;
      if (this.renderer) this.renderer.detach();
      this.attached = false;
      return this;
    };
    /**
     * Add a rule to the current stylesheet.
     * Will insert a rule also after the stylesheet has been rendered first time.
     */

    _proto.addRule = function addRule(name, decl, options) {
      var queue = this.queue; // Plugins can create rules.
      // In order to preserve the right order, we need to queue all `.addRule` calls,
      // which happen after the first `rules.add()` call.

      if (this.attached && !queue) this.queue = [];
      var rule = this.rules.add(name, decl, options);
      if (!rule) return null;
      this.options.jss.plugins.onProcessRule(rule);

      if (this.attached) {
        if (!this.deployed) return rule; // Don't insert rule directly if there is no stringified version yet.
        // It will be inserted all together when .attach is called.

        if (queue) queue.push(rule);
        else {
          this.insertRule(rule);

          if (this.queue) {
            this.queue.forEach(this.insertRule, this);
            this.queue = undefined;
          }
        }
        return rule;
      } // We can't add rules to a detached style node.
      // We will redeploy the sheet once user will attach it.

      this.deployed = false;
      return rule;
    };
    /**
     * Insert rule into the StyleSheet
     */

    _proto.insertRule = function insertRule(rule) {
      if (this.renderer) {
        this.renderer.insertRule(rule);
      }
    };
    /**
     * Create and add rules.
     * Will render also after Style Sheet was rendered the first time.
     */

    _proto.addRules = function addRules(styles, options) {
      var added = [];

      for (var name in styles) {
        var rule = this.addRule(name, styles[name], options);
        if (rule) added.push(rule);
      }

      return added;
    };
    /**
     * Get a rule by name.
     */

    _proto.getRule = function getRule(name) {
      return this.rules.get(name);
    };
    /**
     * Delete a rule by name.
     * Returns `true`: if rule has been deleted from the DOM.
     */

    _proto.deleteRule = function deleteRule(name) {
      var rule = this.rules.get(name);
      if (!rule) return false;
      this.rules.remove(rule);

      if (this.attached && rule.renderable && this.renderer) {
        return this.renderer.deleteRule(rule.renderable);
      }

      return true;
    };
    /**
     * Get index of a rule.
     */

    _proto.indexOf = function indexOf(rule) {
      return this.rules.indexOf(rule);
    };
    /**
     * Deploy pure CSS string to a renderable.
     */

    _proto.deploy = function deploy() {
      if (this.renderer) this.renderer.deploy();
      this.deployed = true;
      return this;
    };
    /**
     * Update the function values with a new data.
     */

    _proto.update = function update() {
      var _this$rules;

      (_this$rules = this.rules).update.apply(_this$rules, arguments);

      return this;
    };
    /**
     * Convert rules to a CSS string.
     */

    _proto.toString = function toString(options) {
      return this.rules.toString(options);
    };

    return StyleSheet;
  })();

var PluginsRegistry =
  /*#__PURE__*/
  (function() {
    function PluginsRegistry() {
      this.plugins = {
        internal: [],
        external: [],
      };
      this.registry = void 0;
    }

    var _proto = PluginsRegistry.prototype;

    /**
     * Call `onCreateRule` hooks and return an object if returned by a hook.
     */
    _proto.onCreateRule = function onCreateRule(name, decl, options) {
      for (var i = 0; i < this.registry.onCreateRule.length; i++) {
        var rule = this.registry.onCreateRule[i](name, decl, options);
        if (rule) return rule;
      }

      return null;
    };
    /**
     * Call `onProcessRule` hooks.
     */

    _proto.onProcessRule = function onProcessRule(rule) {
      if (rule.isProcessed) return;
      var sheet = rule.options.sheet;

      for (var i = 0; i < this.registry.onProcessRule.length; i++) {
        this.registry.onProcessRule[i](rule, sheet);
      }

      if (rule.style) this.onProcessStyle(rule.style, rule, sheet);
      rule.isProcessed = true;
    };
    /**
     * Call `onProcessStyle` hooks.
     */

    _proto.onProcessStyle = function onProcessStyle(style, rule, sheet) {
      for (var i = 0; i < this.registry.onProcessStyle.length; i++) {
        // $FlowFixMe
        rule.style = this.registry.onProcessStyle[i](rule.style, rule, sheet);
      }
    };
    /**
     * Call `onProcessSheet` hooks.
     */

    _proto.onProcessSheet = function onProcessSheet(sheet) {
      for (var i = 0; i < this.registry.onProcessSheet.length; i++) {
        this.registry.onProcessSheet[i](sheet);
      }
    };
    /**
     * Call `onUpdate` hooks.
     */

    _proto.onUpdate = function onUpdate(data, rule, sheet, options) {
      for (var i = 0; i < this.registry.onUpdate.length; i++) {
        this.registry.onUpdate[i](data, rule, sheet, options);
      }
    };
    /**
     * Call `onChangeValue` hooks.
     */

    _proto.onChangeValue = function onChangeValue(value, prop, rule) {
      var processedValue = value;

      for (var i = 0; i < this.registry.onChangeValue.length; i++) {
        processedValue = this.registry.onChangeValue[i](processedValue, prop, rule);
      }

      return processedValue;
    };
    /**
     * Register a plugin.
     */

    _proto.use = function use(newPlugin, options) {
      if (options === void 0) {
        options = {
          queue: 'external',
        };
      }

      var plugins = this.plugins[options.queue]; // Avoids applying same plugin twice, at least based on ref.

      if (plugins.indexOf(newPlugin) !== -1) {
        return;
      }

      plugins.push(newPlugin);
      this.registry = [].concat(this.plugins.external, this.plugins.internal).reduce(
        function(registry, plugin) {
          for (var name in plugin) {
            if (name in registry) {
              registry[name].push(plugin[name]);
            }
          }

          return registry;
        },
        {
          onCreateRule: [],
          onProcessRule: [],
          onProcessStyle: [],
          onProcessSheet: [],
          onChangeValue: [],
          onUpdate: [],
        },
      );
    };

    return PluginsRegistry;
  })();

/**
 * Sheets registry to access them all at one place.
 */
var SheetsRegistry =
  /*#__PURE__*/
  (function() {
    function SheetsRegistry() {
      this.registry = [];
    }

    var _proto = SheetsRegistry.prototype;

    /**
     * Register a Style Sheet.
     */
    _proto.add = function add(sheet) {
      var registry = this.registry;
      var index = sheet.options.index;
      if (registry.indexOf(sheet) !== -1) return;

      if (registry.length === 0 || index >= this.index) {
        registry.push(sheet);
        return;
      } // Find a position.

      for (var i = 0; i < registry.length; i++) {
        if (registry[i].options.index > index) {
          registry.splice(i, 0, sheet);
          return;
        }
      }
    };
    /**
     * Reset the registry.
     */

    _proto.reset = function reset() {
      this.registry = [];
    };
    /**
     * Remove a Style Sheet.
     */

    _proto.remove = function remove(sheet) {
      var index = this.registry.indexOf(sheet);
      this.registry.splice(index, 1);
    };
    /**
     * Convert all attached sheets to a CSS string.
     */

    _proto.toString = function toString(options) {
      return this.registry
        .filter(function(sheet) {
          return sheet.attached;
        })
        .map(function(sheet) {
          return sheet.toString(options);
        })
        .join('\n');
    };

    _createClass(SheetsRegistry, [
      {
        key: 'index',

        /**
         * Current highest index number.
         */
        get: function get() {
          return this.registry.length === 0
            ? 0
            : this.registry[this.registry.length - 1].options.index;
        },
      },
    ]);

    return SheetsRegistry;
  })();

/**
 * This is a global sheets registry. Only DomRenderer will add sheets to it.
 * On the server one should use an own SheetsRegistry instance and add the
 * sheets to it, because you need to make sure to create a new registry for
 * each request in order to not leak sheets across requests.
 */

var sheets = new SheetsRegistry();

var global$1 =
  typeof global !== 'undefined'
    ? global
    : typeof self !== 'undefined'
    ? self
    : typeof window !== 'undefined'
    ? window
    : {};

var ns = '2f1acc6c3a606b082e5eef5e54414ffb';
if (global$1[ns] == null) global$1[ns] = 0; // Bundle may contain multiple JSS versions at the same time. In order to identify
// the current version with just one short number and use it for classes generation
// we use a counter. Also it is more accurate, because user can manually reevaluate
// the module.

var moduleId = global$1[ns]++;

/**
 * Returns a function which generates unique class names based on counters.
 * When new generator function is created, rule counter is reseted.
 * We need to reset the rule counter for SSR for each request.
 */
var createGenerateId = function createGenerateId(options) {
  if (options === void 0) {
    options = {};
  }

  var ruleCounter = 0;
  var defaultPrefix = options.minify ? 'c' : '';
  return function(rule, sheet) {
    ruleCounter += 1;

    var prefix = defaultPrefix;
    var jssId = '';

    if (sheet) {
      prefix = sheet.options.classNamePrefix || defaultPrefix;
      if (sheet.options.jss.id != null) jssId += sheet.options.jss.id;
    }

    if (options.minify) {
      return '' + prefix + moduleId + jssId + ruleCounter;
    }

    return prefix + rule.key + '-' + moduleId + (jssId && '-' + jssId) + '-' + ruleCounter;
  };
};

/**
 * Cache the value from the first time a function is called.
 */
var memoize = function memoize(fn) {
  var value;
  return function() {
    if (!value) value = fn();
    return value;
  };
};
/**
 * Get a style property value.
 */

function getPropertyValue(cssRule, prop) {
  try {
    // Support CSSTOM.
    if (cssRule.attributeStyleMap) {
      return cssRule.attributeStyleMap.get(prop);
    }

    return cssRule.style.getPropertyValue(prop);
  } catch (err) {
    // IE may throw if property is unknown.
    return '';
  }
}
/**
 * Set a style property.
 */

function setProperty(cssRule, prop, value) {
  try {
    var cssValue = value;

    if (Array.isArray(value)) {
      cssValue = toCssValue(value, true);

      if (value[value.length - 1] === '!important') {
        cssRule.style.setProperty(prop, cssValue, 'important');
        return true;
      }
    } // Support CSSTOM.

    if (cssRule.attributeStyleMap) {
      cssRule.attributeStyleMap.set(prop, cssValue);
    } else {
      cssRule.style.setProperty(prop, cssValue);
    }
  } catch (err) {
    // IE may throw if property is unknown.
    return false;
  }

  return true;
}
/**
 * Remove a style property.
 */

function removeProperty(cssRule, prop) {
  try {
    // Support CSSTOM.
    if (cssRule.attributeStyleMap) {
      cssRule.attributeStyleMap.delete(prop);
    } else {
      cssRule.style.removeProperty(prop);
    }
  } catch (err) {}
}
/**
 * Set the selector.
 */

function setSelector(cssRule, selectorText) {
  cssRule.selectorText = selectorText; // Return false if setter was not successful.
  // Currently works in chrome only.

  return cssRule.selectorText === selectorText;
}
/**
 * Gets the `head` element upon the first call and caches it.
 * We assume it can't be null.
 */

var getHead = memoize(function() {
  return document.querySelector('head');
});
/**
 * Find attached sheet with an index higher than the passed one.
 */

function findHigherSheet(registry, options) {
  for (var i = 0; i < registry.length; i++) {
    var sheet = registry[i];

    if (
      sheet.attached &&
      sheet.options.index > options.index &&
      sheet.options.insertionPoint === options.insertionPoint
    ) {
      return sheet;
    }
  }

  return null;
}
/**
 * Find attached sheet with the highest index.
 */

function findHighestSheet(registry, options) {
  for (var i = registry.length - 1; i >= 0; i--) {
    var sheet = registry[i];

    if (sheet.attached && sheet.options.insertionPoint === options.insertionPoint) {
      return sheet;
    }
  }

  return null;
}
/**
 * Find a comment with "jss" inside.
 */

function findCommentNode(text) {
  var head = getHead();

  for (var i = 0; i < head.childNodes.length; i++) {
    var node = head.childNodes[i];

    if (node.nodeType === 8 && node.nodeValue.trim() === text) {
      return node;
    }
  }

  return null;
}

/**
 * Find a node before which we can insert the sheet.
 */
function findPrevNode(options) {
  var registry = sheets.registry;

  if (registry.length > 0) {
    // Try to insert before the next higher sheet.
    var sheet = findHigherSheet(registry, options);

    if (sheet && sheet.renderer) {
      return {
        parent: sheet.renderer.element.parentNode,
        node: sheet.renderer.element,
      };
    } // Otherwise insert after the last attached.

    sheet = findHighestSheet(registry, options);

    if (sheet && sheet.renderer) {
      return {
        parent: sheet.renderer.element.parentNode,
        node: sheet.renderer.element.nextSibling,
      };
    }
  } // Try to find a comment placeholder if registry is empty.

  var insertionPoint = options.insertionPoint;

  if (insertionPoint && typeof insertionPoint === 'string') {
    var comment = findCommentNode(insertionPoint);

    if (comment) {
      return {
        parent: comment.parentNode,
        node: comment.nextSibling,
      };
    } // If user specifies an insertion point and it can't be found in the document -
  }

  return false;
}
/**
 * Insert style element into the DOM.
 */

function insertStyle(style, options) {
  var insertionPoint = options.insertionPoint;
  var nextNode = findPrevNode(options);

  if (nextNode !== false && nextNode.parent) {
    nextNode.parent.insertBefore(style, nextNode.node);
    return;
  } // Works with iframes and any node types.

  if (insertionPoint && typeof insertionPoint.nodeType === 'number') {
    // https://stackoverflow.com/questions/41328728/force-casting-in-flow
    var insertionPointElement = insertionPoint;
    var parentNode = insertionPointElement.parentNode;
    if (parentNode) parentNode.insertBefore(style, insertionPointElement.nextSibling);
    return;
  }

  getHead().appendChild(style);
}
/**
 * Read jss nonce setting from the page if the user has set it.
 */

var getNonce = memoize(function() {
  var node = document.querySelector('meta[property="csp-nonce"]');
  return node ? node.getAttribute('content') : null;
});

var _insertRule = function insertRule(container, rule, index) {
  if (index === void 0) {
    index = container.cssRules.length;
  }

  try {
    if ('insertRule' in container) {
      var c = container;
      c.insertRule(rule, index);
    } // Keyframes rule.
    else if ('appendRule' in container) {
      var _c = container;

      _c.appendRule(rule);
    }
  } catch (err) {
    return false;
  }

  return container.cssRules[index];
};

var createStyle = function createStyle() {
  var el = document.createElement('style'); // Without it, IE will have a broken source order specificity if we
  // insert rules after we insert the style tag.
  // It seems to kick-off the source order specificity algorithm.

  el.textContent = '\n';
  return el;
};

var DomRenderer =
  /*#__PURE__*/
  (function() {
    // HTMLStyleElement needs fixing https://github.com/facebook/flow/issues/2696
    function DomRenderer(sheet) {
      this.getPropertyValue = getPropertyValue;
      this.setProperty = setProperty;
      this.removeProperty = removeProperty;
      this.setSelector = setSelector;
      this.element = void 0;
      this.sheet = void 0;
      this.hasInsertedRules = false;
      // There is no sheet when the renderer is used from a standalone StyleRule.
      if (sheet) sheets.add(sheet);
      this.sheet = sheet;

      var _ref = this.sheet ? this.sheet.options : {},
        media = _ref.media,
        meta = _ref.meta,
        element = _ref.element;

      this.element = element || createStyle();
      this.element.setAttribute('data-jss', '');
      if (media) this.element.setAttribute('media', media);
      if (meta) this.element.setAttribute('data-meta', meta);
      var nonce = getNonce();
      if (nonce) this.element.setAttribute('nonce', nonce);
    }
    /**
     * Insert style element into render tree.
     */

    var _proto = DomRenderer.prototype;

    _proto.attach = function attach() {
      // In the case the element node is external and it is already in the DOM.
      if (this.element.parentNode || !this.sheet) return;
      insertStyle(this.element, this.sheet.options); // When rules are inserted using `insertRule` API, after `sheet.detach().attach()`
      // browsers remove those rules.
      // TODO figure out if its a bug and if it is known.
      // Workaround is to redeploy the sheet.

      if (this.hasInsertedRules) {
        this.hasInsertedRules = false;
        this.deploy();
      }
    };
    /**
     * Remove style element from render tree.
     */

    _proto.detach = function detach() {
      this.element.parentNode.removeChild(this.element);
    };
    /**
     * Inject CSS string into element.
     */

    _proto.deploy = function deploy() {
      var sheet = this.sheet;
      if (!sheet) return;

      if (sheet.options.link) {
        this.insertRules(sheet.rules);
        return;
      }

      this.element.textContent = '\n' + sheet.toString() + '\n';
    };
    /**
     * Insert RuleList into an element.
     */

    _proto.insertRules = function insertRules(rules, nativeParent) {
      for (var i = 0; i < rules.index.length; i++) {
        this.insertRule(rules.index[i], i, nativeParent);
      }
    };
    /**
     * Insert a rule into element.
     */

    _proto.insertRule = function insertRule(rule, index, nativeParent) {
      if (nativeParent === void 0) {
        nativeParent = this.element.sheet;
      }

      if (rule.rules) {
        var parent = rule;
        var latestNativeParent = nativeParent;

        if (rule.type === 'conditional' || rule.type === 'keyframes') {
          // We need to render the container without children first.
          latestNativeParent = _insertRule(
            nativeParent,
            parent.toString({
              children: false,
            }),
            index,
          );

          if (latestNativeParent === false) {
            return false;
          }
        }

        this.insertRules(parent.rules, latestNativeParent);
        return latestNativeParent;
      }

      var ruleStr = rule.toString();
      if (!ruleStr) return false;

      var nativeRule = _insertRule(nativeParent, ruleStr, index);

      if (nativeRule === false) {
        return false;
      }

      this.hasInsertedRules = true;
      rule.renderable = nativeRule;
      return nativeRule;
    };
    /**
     * Delete a rule.
     */

    _proto.deleteRule = function deleteRule(cssRule) {
      var sheet = this.element.sheet;
      var index = this.indexOf(cssRule);
      if (index === -1) return false;
      sheet.deleteRule(index);
      return true;
    };
    /**
     * Get index of a CSS Rule.
     */

    _proto.indexOf = function indexOf(cssRule) {
      var cssRules = this.element.sheet.cssRules;

      for (var index = 0; index < cssRules.length; index++) {
        if (cssRule === cssRules[index]) return index;
      }

      return -1;
    };
    /**
     * Generate a new CSS rule and replace the existing one.
     *
     * Only used for some old browsers because they can't set a selector.
     */

    _proto.replaceRule = function replaceRule(cssRule, rule) {
      var index = this.indexOf(cssRule);
      if (index === -1) return false;
      this.element.sheet.deleteRule(index);
      return this.insertRule(rule, index);
    };
    /**
     * Get all rules elements.
     */

    _proto.getRules = function getRules() {
      return this.element.sheet.cssRules;
    };

    return DomRenderer;
  })();

var instanceCounter = 0;

var Jss =
  /*#__PURE__*/
  (function() {
    function Jss(options) {
      this.id = instanceCounter++;
      this.version = '10.0.0-alpha.17';
      this.plugins = new PluginsRegistry();
      this.options = {
        id: {
          minify: false,
        },
        createGenerateId: createGenerateId,
        Renderer: isBrowser ? DomRenderer : null,
        plugins: [],
      };
      this.generateId = createGenerateId({
        minify: false,
      });

      for (var i = 0; i < plugins.length; i++) {
        this.plugins.use(plugins[i], {
          queue: 'internal',
        });
      }

      this.setup(options);
    }
    /**
     * Prepares various options, applies plugins.
     * Should not be used twice on the same instance, because there is no plugins
     * deduplication logic.
     */

    var _proto = Jss.prototype;

    _proto.setup = function setup(options) {
      if (options === void 0) {
        options = {};
      }

      if (options.createGenerateId) {
        this.options.createGenerateId = options.createGenerateId;
      }

      if (options.id) {
        this.options.id = _extends$1({}, this.options.id, options.id);
      }

      if (options.createGenerateId || options.id) {
        this.generateId = this.options.createGenerateId(this.options.id);
      }

      if (options.insertionPoint != null) this.options.insertionPoint = options.insertionPoint;

      if ('Renderer' in options) {
        this.options.Renderer = options.Renderer;
      } // eslint-disable-next-line prefer-spread

      if (options.plugins) this.use.apply(this, options.plugins);
      return this;
    };
    /**
     * Create a Style Sheet.
     */

    _proto.createStyleSheet = function createStyleSheet(styles, options) {
      if (options === void 0) {
        options = {};
      }

      var _options = options,
        index = _options.index;

      if (typeof index !== 'number') {
        index = sheets.index === 0 ? 0 : sheets.index + 1;
      }

      var sheet = new StyleSheet(
        styles,
        _extends$1({}, options, {
          jss: this,
          generateId: options.generateId || this.generateId,
          insertionPoint: this.options.insertionPoint,
          Renderer: this.options.Renderer,
          index: index,
        }),
      );
      this.plugins.onProcessSheet(sheet);
      return sheet;
    };
    /**
     * Detach the Style Sheet and remove it from the registry.
     */

    _proto.removeStyleSheet = function removeStyleSheet(sheet) {
      sheet.detach();
      sheets.remove(sheet);
      return this;
    };
    /**
     * Create a rule without a Style Sheet.
     */

    _proto.createRule = function createRule$$1(name, style, options) {
      if (style === void 0) {
        style = {};
      }

      if (options === void 0) {
        options = {};
      }

      // Enable rule without name for inline styles.
      if (typeof name === 'object') {
        return this.createRule(undefined, name, style);
      }

      var ruleOptions = _extends$1({}, options, {
        jss: this,
        Renderer: this.options.Renderer,
      });

      if (!ruleOptions.generateId) ruleOptions.generateId = this.generateId;
      if (!ruleOptions.classes) ruleOptions.classes = {};
      if (!ruleOptions.keyframes) ruleOptions.keyframes = {};

      var rule = createRule(name, style, ruleOptions);

      if (rule) this.plugins.onProcessRule(rule);
      return rule;
    };
    /**
     * Register plugin. Passed function will be invoked with a rule instance.
     */

    _proto.use = function use() {
      var _this = this;

      for (
        var _len = arguments.length, plugins$$1 = new Array(_len), _key = 0;
        _key < _len;
        _key++
      ) {
        plugins$$1[_key] = arguments[_key];
      }

      plugins$$1.forEach(function(plugin) {
        _this.plugins.use(plugin);
      });
      return this;
    };

    return Jss;
  })();

/**
 * Extracts a styles object with only props that contain function values.
 */
function getDynamicStyles(styles) {
  var to = null;

  for (var key in styles) {
    var value = styles[key];
    var type = typeof value;

    if (type === 'function') {
      if (!to) to = {};
      to[key] = value;
    } else if (type === 'object' && value !== null && !Array.isArray(value)) {
      var extracted = getDynamicStyles(value);

      if (extracted) {
        if (!to) to = {};
        to[key] = extracted;
      }
    }
  }

  return to;
}

/**
 * A better abstraction over CSS.
 *
 * @copyright Oleg Isonen (Slobodskoi) / Isonen 2014-present
 * @website https://github.com/cssinjs/jss
 * @license MIT
 */

/**
 * Export a constant indicating if this browser has CSSTOM support.
 * https://developers.google.com/web/updates/2018/03/cssom
 */
var hasCSSTOMSupport = typeof CSS !== 'undefined' && CSS && 'number' in CSS;
/**
 * Creates a new instance of Jss.
 */

var create = function create(options) {
  return new Jss(options);
};
/**
 * A global Jss instance.
 */

var index = create();

var now = Date.now();
var fnValuesNs = 'fnValues' + now;
var fnRuleNs = 'fnStyle' + ++now;
function functionPlugin() {
  return {
    onCreateRule: function onCreateRule(name, decl, options) {
      if (typeof decl !== 'function') return null;
      var rule = createRule(name, {}, options);
      rule[fnRuleNs] = decl;
      return rule;
    },
    onProcessStyle: function onProcessStyle(style, rule) {
      // We need to extract function values from the declaration, so that we can keep core unaware of them.
      // We need to do that only once.
      // We don't need to extract functions on each style update, since this can happen only once.
      // We don't support function values inside of function rules.
      if (fnValuesNs in rule || fnRuleNs in rule) return style;
      var fnValues = {};

      for (var prop in style) {
        var value = style[prop];
        if (typeof value !== 'function') continue;
        delete style[prop];
        fnValues[prop] = value;
      } // $FlowFixMe

      rule[fnValuesNs] = fnValues;
      return style;
    },
    onUpdate: function onUpdate(data, rule, sheet, options) {
      var styleRule = rule;
      var fnRule = styleRule[fnRuleNs]; // If we have a style function, the entire rule is dynamic and style object
      // will be returned from that function.

      if (fnRule) {
        styleRule.style = fnRule(data);
      }

      var fnValues = styleRule[fnValuesNs]; // If we have a fn values map, it is a rule with function values.

      if (fnValues) {
        for (var prop in fnValues) {
          styleRule.prop(prop, fnValues[prop](data), options);
        }
      }
    },
  };
}

var at = '@global';
var atPrefix = '@global ';

var GlobalContainerRule =
  /*#__PURE__*/
  (function() {
    function GlobalContainerRule(key, styles, options) {
      this.type = 'global';
      this.at = at;
      this.rules = void 0;
      this.options = void 0;
      this.key = void 0;
      this.isProcessed = false;
      this.key = key;
      this.options = options;
      this.rules = new RuleList(
        _extends$1({}, options, {
          parent: this,
        }),
      );

      for (var selector in styles) {
        this.rules.add(selector, styles[selector]);
      }

      this.rules.process();
    }
    /**
     * Get a rule.
     */

    var _proto = GlobalContainerRule.prototype;

    _proto.getRule = function getRule(name) {
      return this.rules.get(name);
    };
    /**
     * Create and register rule, run plugins.
     */

    _proto.addRule = function addRule(name, style, options) {
      var rule = this.rules.add(name, style, options);
      this.options.jss.plugins.onProcessRule(rule);
      return rule;
    };
    /**
     * Get index of a rule.
     */

    _proto.indexOf = function indexOf(rule) {
      return this.rules.indexOf(rule);
    };
    /**
     * Generates a CSS string.
     */

    _proto.toString = function toString() {
      return this.rules.toString();
    };

    return GlobalContainerRule;
  })();

var GlobalPrefixedRule =
  /*#__PURE__*/
  (function() {
    function GlobalPrefixedRule(key, style, options) {
      this.type = 'global';
      this.at = at;
      this.options = void 0;
      this.rule = void 0;
      this.isProcessed = false;
      this.key = void 0;
      this.key = key;
      this.options = options;
      var selector = key.substr(atPrefix.length);
      this.rule = options.jss.createRule(
        selector,
        style,
        _extends$1({}, options, {
          parent: this,
        }),
      );
    }

    var _proto2 = GlobalPrefixedRule.prototype;

    _proto2.toString = function toString(options) {
      return this.rule ? this.rule.toString(options) : '';
    };

    return GlobalPrefixedRule;
  })();

var separatorRegExp = /\s*,\s*/g;

function addScope(selector, scope) {
  var parts = selector.split(separatorRegExp);
  var scoped = '';

  for (var i = 0; i < parts.length; i++) {
    scoped += scope + ' ' + parts[i].trim();
    if (parts[i + 1]) scoped += ', ';
  }

  return scoped;
}

function handleNestedGlobalContainerRule(rule) {
  var options = rule.options,
    style = rule.style;
  var rules = style ? style[at] : null;
  if (!rules) return;

  for (var name in rules) {
    options.sheet.addRule(
      name,
      rules[name],
      _extends$1({}, options, {
        selector: addScope(name, rule.selector),
      }),
    );
  }

  delete style[at];
}

function handlePrefixedGlobalRule(rule) {
  var options = rule.options,
    style = rule.style;

  for (var prop in style) {
    if (prop[0] !== '@' || prop.substr(0, at.length) !== at) continue;
    var selector = addScope(prop.substr(at.length), rule.selector);
    options.sheet.addRule(
      selector,
      style[prop],
      _extends$1({}, options, {
        selector: selector,
      }),
    );
    delete style[prop];
  }
}
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */

function jssGlobal() {
  function onCreateRule(name, styles, options) {
    if (!name) return null;

    if (name === at) {
      return new GlobalContainerRule(name, styles, options);
    }

    if (name[0] === '@' && name.substr(0, atPrefix.length) === atPrefix) {
      return new GlobalPrefixedRule(name, styles, options);
    }

    var parent = options.parent;

    if (parent) {
      if (
        parent.type === 'global' ||
        (parent.options.parent && parent.options.parent.type === 'global')
      ) {
        options.scoped = false;
      }
    }

    if (options.scoped === false) {
      options.selector = name;
    }

    return null;
  }

  function onProcessRule(rule) {
    if (rule.type !== 'style') return;
    handleNestedGlobalContainerRule(rule);
    handlePrefixedGlobalRule(rule);
  }

  return {
    onCreateRule: onCreateRule,
    onProcessRule: onProcessRule,
  };
}

var separatorRegExp$1 = /\s*,\s*/g;
var parentRegExp = /&/g;
var refRegExp$1 = /\$([\w-]+)/g;
/**
 * Convert nested rules to separate, remove them from original styles.
 *
 * @param {Rule} rule
 * @api public
 */

function jssNested() {
  // Get a function to be used for $ref replacement.
  function getReplaceRef(container, sheet) {
    return function(match, key) {
      var rule = container.getRule(key) || (sheet && sheet.getRule(key));

      if (rule) {
        rule = rule;
        return rule.selector;
      }
      return key;
    };
  }

  function replaceParentRefs(nestedProp, parentProp) {
    var parentSelectors = parentProp.split(separatorRegExp$1);
    var nestedSelectors = nestedProp.split(separatorRegExp$1);
    var result = '';

    for (var i = 0; i < parentSelectors.length; i++) {
      var parent = parentSelectors[i];

      for (var j = 0; j < nestedSelectors.length; j++) {
        var nested = nestedSelectors[j];
        if (result) result += ', '; // Replace all & by the parent or prefix & with the parent.

        result +=
          nested.indexOf('&') !== -1 ? nested.replace(parentRegExp, parent) : parent + ' ' + nested;
      }
    }

    return result;
  }

  function getOptions(rule, container, options) {
    // Options has been already created, now we only increase index.
    if (options)
      return _extends$1({}, options, {
        index: options.index + 1,
      });
    var nestingLevel = rule.options.nestingLevel;
    nestingLevel = nestingLevel === undefined ? 1 : nestingLevel + 1;
    return _extends$1({}, rule.options, {
      nestingLevel: nestingLevel,
      index: container.indexOf(rule) + 1,
    });
  }

  function onProcessStyle(style, rule, sheet) {
    if (rule.type !== 'style') return style;
    var styleRule = rule;
    var container = styleRule.options.parent;
    var options;
    var replaceRef;

    for (var prop in style) {
      var isNested = prop.indexOf('&') !== -1;
      var isNestedConditional = prop[0] === '@';
      if (!isNested && !isNestedConditional) continue;
      options = getOptions(styleRule, container, options);

      if (isNested) {
        var selector = replaceParentRefs(prop, styleRule.selector); // Lazily create the ref replacer function just once for
        // all nested rules within the sheet.

        if (!replaceRef) replaceRef = getReplaceRef(container, sheet); // Replace all $refs.

        selector = selector.replace(refRegExp$1, replaceRef);
        container.addRule(
          selector,
          style[prop],
          _extends$1({}, options, {
            selector: selector,
          }),
        );
      } else if (isNestedConditional) {
        // Place conditional right after the parent rule to ensure right ordering.
        container
          .addRule(prop, {}, options) // Flow expects more options but they aren't required
          // And flow doesn't know this will always be a StyleRule which has the addRule method
          // $FlowFixMe
          .addRule(styleRule.key, style[prop], {
            selector: styleRule.selector,
          });
      }

      delete style[prop];
    }

    return style;
  }

  return {
    onProcessStyle: onProcessStyle,
  };
}

/* eslint-disable no-var, prefer-template */
var uppercasePattern = /[A-Z]/g;
var msPattern = /^ms-/;
var cache = {};

function toHyphenLower(match) {
  return '-' + match.toLowerCase();
}

function hyphenateStyleName(name) {
  if (cache.hasOwnProperty(name)) {
    return cache[name];
  }

  var hName = name.replace(uppercasePattern, toHyphenLower);
  return (cache[name] = msPattern.test(hName) ? '-' + hName : hName);
}

/**
 * Convert camel cased property names to dash separated.
 *
 * @param {Object} style
 * @return {Object}
 */

function convertCase(style) {
  var converted = {};

  for (var prop in style) {
    var key = prop.indexOf('--') === 0 ? prop : hyphenateStyleName(prop);
    converted[key] = style[prop];
  }

  if (style.fallbacks) {
    if (Array.isArray(style.fallbacks)) converted.fallbacks = style.fallbacks.map(convertCase);
    else converted.fallbacks = convertCase(style.fallbacks);
  }

  return converted;
}
/**
 * Allow camel cased property names by converting them back to dasherized.
 *
 * @param {Rule} rule
 */

function camelCase() {
  function onProcessStyle(style) {
    if (Array.isArray(style)) {
      // Handle rules like @font-face, which can have multiple styles in an array
      for (var index = 0; index < style.length; index++) {
        style[index] = convertCase(style[index]);
      }

      return style;
    }

    return convertCase(style);
  }

  function onChangeValue(value, prop, rule) {
    if (prop.indexOf('--') === 0) {
      return value;
    }

    var hyphenatedProp = hyphenateStyleName(prop); // There was no camel case in place

    if (prop === hyphenatedProp) return value;
    rule.prop(hyphenatedProp, value); // Core will ignore that property value we set the proper one above.

    return null;
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue,
  };
}

var px = hasCSSTOMSupport ? window.CSS.px : 'px';
var ms = hasCSSTOMSupport ? window.CSS.ms : 'ms';
var percent = hasCSSTOMSupport ? window.CSS.percent : '%';
/**
 * Generated jss-plugin-default-unit CSS property units
 *
 * @type object
 */

var defaultUnits = {
  // Animation properties
  'animation-delay': ms,
  'animation-duration': ms,
  // Background properties
  'background-position': px,
  'background-position-x': px,
  'background-position-y': px,
  'background-size': px,
  // Border Properties
  border: px,
  'border-bottom': px,
  'border-bottom-left-radius': px,
  'border-bottom-right-radius': px,
  'border-bottom-width': px,
  'border-left': px,
  'border-left-width': px,
  'border-radius': px,
  'border-right': px,
  'border-right-width': px,
  'border-top': px,
  'border-top-left-radius': px,
  'border-top-right-radius': px,
  'border-top-width': px,
  'border-width': px,
  // Margin properties
  margin: px,
  'margin-bottom': px,
  'margin-left': px,
  'margin-right': px,
  'margin-top': px,
  // Padding properties
  padding: px,
  'padding-bottom': px,
  'padding-left': px,
  'padding-right': px,
  'padding-top': px,
  // Mask properties
  'mask-position-x': px,
  'mask-position-y': px,
  'mask-size': px,
  // Width and height properties
  height: px,
  width: px,
  'min-height': px,
  'max-height': px,
  'min-width': px,
  'max-width': px,
  // Position properties
  bottom: px,
  left: px,
  top: px,
  right: px,
  // Shadow properties
  'box-shadow': px,
  'text-shadow': px,
  // Column properties
  'column-gap': px,
  'column-rule': px,
  'column-rule-width': px,
  'column-width': px,
  // Font and text properties
  'font-size': px,
  'font-size-delta': px,
  'letter-spacing': px,
  'text-indent': px,
  'text-stroke': px,
  'text-stroke-width': px,
  'word-spacing': px,
  // Motion properties
  motion: px,
  'motion-offset': px,
  // Outline properties
  outline: px,
  'outline-offset': px,
  'outline-width': px,
  // Perspective properties
  perspective: px,
  'perspective-origin-x': percent,
  'perspective-origin-y': percent,
  // Transform properties
  'transform-origin': percent,
  'transform-origin-x': percent,
  'transform-origin-y': percent,
  'transform-origin-z': percent,
  // Transition properties
  'transition-delay': ms,
  'transition-duration': ms,
  // Alignment properties
  'vertical-align': px,
  'flex-basis': px,
  // Some random properties
  'shape-margin': px,
  size: px,
  // Grid properties
  grid: px,
  'grid-gap': px,
  'grid-row-gap': px,
  'grid-column-gap': px,
  'grid-template-rows': px,
  'grid-template-columns': px,
  'grid-auto-rows': px,
  'grid-auto-columns': px,
  // Not existing properties.
  // Used to avoid issues with jss-plugin-expand integration.
  'box-shadow-x': px,
  'box-shadow-y': px,
  'box-shadow-blur': px,
  'box-shadow-spread': px,
  'font-line-height': px,
  'text-shadow-x': px,
  'text-shadow-y': px,
  'text-shadow-blur': px,
};

/**
 * Clones the object and adds a camel cased property version.
 */
function addCamelCasedVersion(obj) {
  var regExp = /(-[a-z])/g;

  var replace = function replace(str) {
    return str[1].toUpperCase();
  };

  var newObj = {};

  for (var _key in obj) {
    newObj[_key] = obj[_key];
    newObj[_key.replace(regExp, replace)] = obj[_key];
  }

  return newObj;
}

var units = addCamelCasedVersion(defaultUnits);
/**
 * Recursive deep style passing function
 */

function iterate(prop, value, options) {
  if (!value) return value;

  if (Array.isArray(value)) {
    for (var i = 0; i < value.length; i++) {
      value[i] = iterate(prop, value[i], options);
    }
  } else if (typeof value === 'object') {
    if (prop === 'fallbacks') {
      for (var innerProp in value) {
        value[innerProp] = iterate(innerProp, value[innerProp], options);
      }
    } else {
      for (var _innerProp in value) {
        value[_innerProp] = iterate(prop + '-' + _innerProp, value[_innerProp], options);
      }
    }
  } else if (typeof value === 'number') {
    if (options[prop]) {
      return '' + value + options[prop];
    }

    if (units[prop]) {
      return typeof units[prop] === 'function'
        ? units[prop](value).toString()
        : '' + value + units[prop];
    }

    return value.toString();
  }

  return value;
}
/**
 * Add unit to numeric values.
 */

function defaultUnit(options) {
  if (options === void 0) {
    options = {};
  }

  var camelCasedOptions = addCamelCasedVersion(options);

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;

    for (var prop in style) {
      style[prop] = iterate(prop, style[prop], camelCasedOptions);
    }

    return style;
  }

  function onChangeValue(value, prop) {
    return iterate(prop, value, camelCasedOptions);
  }

  return {
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue,
  };
}

function _arrayWithoutHoles$1(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }

    return arr2;
  }
}

function _iterableToArray$1(iter) {
  if (
    Symbol.iterator in Object(iter) ||
    Object.prototype.toString.call(iter) === '[object Arguments]'
  )
    return Array.from(iter);
}

function _nonIterableSpread$1() {
  throw new TypeError('Invalid attempt to spread non-iterable instance');
}

function _toConsumableArray$1(arr) {
  return _arrayWithoutHoles$1(arr) || _iterableToArray$1(arr) || _nonIterableSpread$1();
}

// Export javascript style and css style vendor prefixes.
var js = '';
var css = '';
var vendor = '';
var browser = '';
var isTouch = isBrowser && 'ontouchstart' in document.documentElement; // We should not do anything if required serverside.

if (isBrowser) {
  // Order matters. We need to check Webkit the last one because
  // other vendors use to add Webkit prefixes to some properties
  var jsCssMap = {
    Moz: '-moz-',
    ms: '-ms-',
    O: '-o-',
    Webkit: '-webkit-',
  };

  var _document$createEleme = document.createElement('p'),
    style = _document$createEleme.style;

  var testProp = 'Transform';

  for (var key in jsCssMap) {
    if (key + testProp in style) {
      js = key;
      css = jsCssMap[key];
      break;
    }
  } // Correctly detect the Edge browser.

  if (js === 'Webkit' && 'msHyphens' in style) {
    js = 'ms';
    css = jsCssMap.ms;
    browser = 'edge';
  } // Correctly detect the Safari browser.

  if (js === 'Webkit' && '-apple-trailing-word' in style) {
    vendor = 'apple';
  }
}
/**
 * Vendor prefix string for the current browser.
 *
 * @type {{js: String, css: String, vendor: String, browser: String}}
 * @api public
 */

var prefix = {
  js: js,
  css: css,
  vendor: vendor,
  browser: browser,
  isTouch: isTouch,
};

/**
 * Test if a keyframe at-rule should be prefixed or not
 *
 * @param {String} vendor prefix string for the current browser.
 * @return {String}
 * @api public
 */

function supportedKeyframes(key) {
  // Keyframes is already prefixed. e.g. key = '@-webkit-keyframes a'
  if (key[1] === '-') return key; // No need to prefix IE/Edge. Older browsers will ignore unsupported rules.
  // https://caniuse.com/#search=keyframes

  if (prefix.js === 'ms') return key;
  return '@' + prefix.css + 'keyframes' + key.substr(10);
}

// https://caniuse.com/#search=appearance

var appearence = {
  noPrefill: ['appearance'],
  supportedProperty: function supportedProperty(prop) {
    if (prop !== 'appearance') return false;
    if (prefix.js === 'ms') return '-webkit-' + prop;
    return prefix.css + prop;
  },
};

var regExp = /[-\s]+(.)?/g;
/**
 * Replaces the letter with the capital letter
 *
 * @param {String} match
 * @param {String} c
 * @return {String}
 * @api private
 */

function toUpper(match, c) {
  return c ? c.toUpperCase() : '';
}
/**
 * Convert dash separated strings to camel-cased.
 *
 * @param {String} str
 * @return {String}
 * @api private
 */

function camelize(str) {
  return str.replace(regExp, toUpper);
}

/**
 * Convert dash separated strings to pascal cased.
 *
 * @param {String} str
 * @return {String}
 * @api private
 */

function pascalize(str) {
  return camelize('-' + str);
}

// https://caniuse.com/#search=multicolumn
// https://github.com/postcss/autoprefixer/issues/491
// https://github.com/postcss/autoprefixer/issues/177

var breakPropsOld = {
  supportedProperty: function supportedProperty(prop, style) {
    if (!/^break-/.test(prop)) return false;

    if (prefix.js === 'Webkit') {
      var jsProp = 'WebkitColumn' + pascalize(prop);
      return jsProp in style ? prefix.css + 'column-' + prop : false;
    }

    if (prefix.js === 'Moz') {
      var _jsProp = 'page' + pascalize(prop);

      return _jsProp in style ? 'page-' + prop : false;
    }

    return false;
  },
};

// https://caniuse.com/#search=color-adjust

var colorAdjust = {
  noPrefill: ['color-adjust'],
  supportedProperty: function supportedProperty(prop) {
    if (prop !== 'color-adjust') return false;
    if (prefix.js === 'Webkit') return prefix.css + 'print-' + prop;
    return prop;
  },
};

var propMap = {
  flex: 'box-flex',
  'flex-grow': 'box-flex',
  'flex-direction': ['box-orient', 'box-direction'],
  order: 'box-ordinal-group',
  'align-items': 'box-align',
  'flex-flow': ['box-orient', 'box-direction'],
  'justify-content': 'box-pack',
};
var propKeys = Object.keys(propMap);

var prefixCss = function prefixCss(p) {
  return prefix.css + p;
}; // Support old flex spec from 2009.

var flex2009 = {
  supportedProperty: function supportedProperty(prop, style, _ref) {
    var multiple = _ref.multiple;

    if (propKeys.indexOf(prop) > -1) {
      var newProp = propMap[prop];

      if (!Array.isArray(newProp)) {
        return prefix.js + pascalize(newProp) in style ? prefix.css + newProp : false;
      }

      if (!multiple) return false;

      for (var i = 0; i < newProp.length; i++) {
        if (!(prefix.js + pascalize(newProp[0]) in style)) {
          return false;
        }
      }

      return newProp.map(prefixCss);
    }

    return false;
  },
};

var propMap$1 = {
  'flex-grow': 'flex-positive',
  'flex-shrink': 'flex-negative',
  'flex-basis': 'flex-preferred-size',
  'justify-content': 'flex-pack',
  order: 'flex-order',
  'align-items': 'flex-align',
  'align-content': 'flex-line-pack', // 'align-self' is handled by 'align-self' plugin.
  // Support old flex spec from 2012.
};
var flex2012 = {
  supportedProperty: function supportedProperty(prop, style) {
    var newProp = propMap$1[prop];
    if (!newProp) return false;
    return prefix.js + pascalize(newProp) in style ? prefix.css + newProp : false;
  },
};

// See https://github.com/postcss/autoprefixer/issues/324.

var inlineLogicalOld = {
  supportedProperty: function supportedProperty(prop, style) {
    if (!/^(border|margin|padding)-inline/.test(prop)) return false;
    if (prefix.js === 'Moz') return prop;
    var newProp = prop.replace('-inline', '');
    return prefix.js + pascalize(newProp) in style ? prefix.css + newProp : false;
  },
};

// but we can use a longhand property instead.
// https://caniuse.com/#search=mask

var mask = {
  noPrefill: ['mask'],
  supportedProperty: function supportedProperty(prop, style) {
    if (!/^mask/.test(prop)) return false;

    if (prefix.js === 'Webkit') {
      var longhand = 'mask-image';

      if (camelize(longhand) in style) {
        return prop;
      }

      if (prefix.js + pascalize(longhand) in style) {
        return prefix.css + prop;
      }
    }

    return prop;
  },
};

// https://caniuse.com/#search=overscroll-behavior

var overscrollBehavior = {
  supportedProperty: function supportedProperty(prop) {
    if (prop !== 'overscroll-behavior') return false;

    if (prefix.js === 'ms') {
      return prefix.css + 'scroll-chaining';
    }

    return prop;
  },
};

var prefixed = {
  supportedProperty: function supportedProperty(prop, style) {
    var pascalized = pascalize(prop); // Return custom CSS variable without prefixing.

    if (prop[0] === '-') return prop; // Return already prefixed value without prefixing.

    if (prop[0] === '-' && prop[1] === '-') return prop;
    if (prefix.js + pascalized in style) return prefix.css + prop; // Try webkit fallback.

    if (prefix.js !== 'Webkit' && 'Webkit' + pascalized in style) return '-webkit-' + prop;
    return false;
  },
};

// https://caniuse.com/#search=scroll-snap

var scrollSnap = {
  supportedProperty: function supportedProperty(prop) {
    if (prop.substring(0, 11) !== 'scroll-snap') return false;

    if (prefix.js === 'ms') {
      return '' + prefix.css + prop;
    }

    return prop;
  },
};

// https://caniuse.com/#search=text-orientation

var textOrientation = {
  noPrefill: ['text-orientation'],
  supportedProperty: function supportedProperty(prop) {
    if (prop !== 'text-orientation') return false;

    if (prefix.vendor === 'apple' && !prefix.isTouch) {
      return prefix.css + prop;
    }

    return prop;
  },
};

// https://caniuse.com/#search=transform

var transform = {
  noPrefill: ['transform'],
  supportedProperty: function supportedProperty(prop, style, options) {
    if (prop !== 'transform') return false;

    if (options.transform) {
      return prop;
    }

    return prefix.css + prop;
  },
};

// https://caniuse.com/#search=transition

var transition = {
  noPrefill: ['transition'],
  supportedProperty: function supportedProperty(prop, style, options) {
    if (prop !== 'transition') return false;

    if (options.transition) {
      return prop;
    }

    return prefix.css + prop;
  },
};

// Camelization is required because we can't test using.
// CSS syntax for e.g. in FF.

var unprefixed = {
  supportedProperty: function supportedProperty(prop, style) {
    return camelize(prop) in style ? prop : false;
  },
};

// https://caniuse.com/#search=writing-mode

var writingMode = {
  noPrefill: ['writing-mode'],
  supportedProperty: function supportedProperty(prop) {
    if (prop !== 'writing-mode') return false;

    if (prefix.js === 'Webkit' || prefix.js === 'ms') {
      return prefix.css + prop;
    }

    return prop;
  },
};

// plugins = [
//   ...plugins,
//    breakPropsOld,
//    inlineLogicalOld,
//    unprefixed,
//    prefixed,
//    scrollSnap,
//    flex2012,
//    flex2009
// ]
// Plugins without 'noPrefill' value, going last.
// 'flex-*' plugins should be at the bottom.
// 'flex2009' going after 'flex2012'.
// 'prefixed' going after 'unprefixed'

var plugins$1 = [
  appearence,
  colorAdjust,
  mask,
  textOrientation,
  transform,
  transition,
  writingMode,
  breakPropsOld,
  inlineLogicalOld,
  unprefixed,
  prefixed,
  scrollSnap,
  overscrollBehavior,
  flex2012,
  flex2009,
];
var propertyDetectors = plugins$1
  .filter(function(p) {
    return p.supportedProperty;
  })
  .map(function(p) {
    return p.supportedProperty;
  });
var noPrefill = plugins$1
  .filter(function(p) {
    return p.noPrefill;
  })
  .reduce(function(a, p) {
    a.push.apply(a, _toConsumableArray$1(p.noPrefill));
    return a;
  }, []);

var el;
var cache$1 = {};

if (isBrowser) {
  el = document.createElement('p'); // We test every property on vendor prefix requirement.
  // Once tested, result is cached. It gives us up to 70% perf boost.
  // http://jsperf.com/element-style-object-access-vs-plain-object
  //
  // Prefill cache with known css properties to reduce amount of
  // properties we need to feature test at runtime.
  // http://davidwalsh.name/vendor-prefix

  var computed = window.getComputedStyle(document.documentElement, '');

  for (var key$1 in computed) {
    // eslint-disable-next-line no-restricted-globals
    if (!isNaN(key$1)) cache$1[computed[key$1]] = computed[key$1];
  } // Properties that cannot be correctly detected using the
  // cache prefill method.

  noPrefill.forEach(function(x) {
    return delete cache$1[x];
  });
}
/**
 * Test if a property is supported, returns supported property with vendor
 * prefix if required. Returns `false` if not supported.
 *
 * @param {String} prop dash separated
 * @param {Object} [options]
 * @return {String|Boolean}
 * @api public
 */

function supportedProperty(prop, options) {
  if (options === void 0) {
    options = {};
  }

  // For server-side rendering.
  if (!el) return prop; // Remove cache for benchmark tests or return property from the cache.

  if (cache$1[prop] != null) {
    return cache$1[prop];
  } // Check if 'transition' or 'transform' natively supported in browser.

  if (prop === 'transition' || prop === 'transform') {
    options[prop] = prop in el.style;
  } // Find a plugin for current prefix property.

  for (var i = 0; i < propertyDetectors.length; i++) {
    cache$1[prop] = propertyDetectors[i](prop, el.style, options); // Break loop, if value found.

    if (cache$1[prop]) break;
  } // Reset styles for current property.
  // Firefox can even throw an error for invalid properties, e.g., "0".

  try {
    el.style[prop] = '';
  } catch (err) {
    return false;
  }

  return cache$1[prop];
}

var cache$1$1 = {};
var transitionProperties = {
  transition: 1,
  'transition-property': 1,
  '-webkit-transition': 1,
  '-webkit-transition-property': 1,
};
var transPropsRegExp = /(^\s*[\w-]+)|, (\s*[\w-]+)(?![^()]*\))/g;
var el$1;
/**
 * Returns prefixed value transition/transform if needed.
 *
 * @param {String} match
 * @param {String} p1
 * @param {String} p2
 * @return {String}
 * @api private
 */

function prefixTransitionCallback(match, p1, p2) {
  if (p1 === 'var') return 'var';
  if (p1 === 'all') return 'all';
  if (p2 === 'all') return ', all';
  return p1 ? supportedProperty(p1) : ', ' + supportedProperty(p2);
}

if (isBrowser) el$1 = document.createElement('p');
/**
 * Returns prefixed value if needed. Returns `false` if value is not supported.
 *
 * @param {String} property
 * @param {String} value
 * @return {String|Boolean}
 * @api public
 */

function supportedValue(property, value) {
  // For server-side rendering.
  var prefixedValue = value;
  if (!el$1) return value; // It is a string or a number as a string like '1'.
  // We want only prefixable values here.
  // eslint-disable-next-line no-restricted-globals

  if (typeof prefixedValue !== 'string' || !isNaN(parseInt(prefixedValue, 10))) {
    return prefixedValue;
  } // Create cache key for current value.

  var cacheKey = property + prefixedValue; // Remove cache for benchmark tests or return value from cache.

  if (cache$1$1[cacheKey] != null) {
    return cache$1$1[cacheKey];
  } // IE can even throw an error in some cases, for e.g. style.content = 'bar'.

  try {
    // Test value as it is.
    el$1.style[property] = prefixedValue;
  } catch (err) {
    // Return false if value not supported.
    cache$1$1[cacheKey] = false;
    return false;
  } // If 'transition' or 'transition-property' property.

  if (transitionProperties[property]) {
    prefixedValue = prefixedValue.replace(transPropsRegExp, prefixTransitionCallback);
  } else if (el$1.style[property] === '') {
    // Value with a vendor prefix.
    prefixedValue = prefix.css + prefixedValue; // Hardcode test to convert "flex" to "-ms-flexbox" for IE10.

    if (prefixedValue === '-ms-flex') el$1.style[property] = '-ms-flexbox'; // Test prefixed value.

    el$1.style[property] = prefixedValue; // Return false if value not supported.

    if (el$1.style[property] === '') {
      cache$1$1[cacheKey] = false;
      return false;
    }
  } // Reset styles for current property.

  el$1.style[property] = ''; // Write current value to cache.

  cache$1$1[cacheKey] = prefixedValue;
  return cache$1$1[cacheKey];
}

/**
 * Add vendor prefix to a property name when needed.
 *
 * @api public
 */

function jssVendorPrefixer() {
  function onProcessRule(rule) {
    if (rule.type === 'keyframes') {
      var atRule = rule;
      atRule.at = supportedKeyframes(atRule.at);
    }
  }

  function onProcessStyle(style, rule) {
    if (rule.type !== 'style') return style;

    for (var prop in style) {
      var value = style[prop];
      var changeProp = false;
      var supportedProp = supportedProperty(prop);
      if (supportedProp && supportedProp !== prop) changeProp = true;
      var changeValue = false;
      var supportedValue$$1 = supportedValue(supportedProp, toCssValue(value));
      if (supportedValue$$1 && supportedValue$$1 !== value) changeValue = true;

      if (changeProp || changeValue) {
        if (changeProp) delete style[prop];
        style[supportedProp || prop] = supportedValue$$1 || value;
      }
    }

    return style;
  }

  function onChangeValue(value, prop) {
    return supportedValue(prop, toCssValue(value)) || value;
  }

  return {
    onProcessRule: onProcessRule,
    onProcessStyle: onProcessStyle,
    onChangeValue: onChangeValue,
  };
}

/**
 * Sort props by length.
 */
function jssPropsSort() {
  var sort = function sort(prop0, prop1) {
    if (prop0.length === prop1.length) {
      return prop0 > prop1 ? 1 : -1;
    }

    return prop0.length - prop1.length;
  };

  return {
    onProcessStyle: function onProcessStyle(style, rule) {
      if (rule.type !== 'style') return style;
      var newStyle = {};
      var props = Object.keys(style).sort(sort);

      for (var i = 0; i < props.length; i++) {
        newStyle[props[i]] = style[props[i]];
      }

      return newStyle;
    },
  };
}

function jssPreset() {
  return {
    plugins: [
      functionPlugin(),
      jssGlobal(),
      jssNested(),
      camelCase(),
      defaultUnit(), // Disable the vendor prefixer server-side, it does nothing.
      // This way, we can get a performance boost.
      // In the documentation, we are using `autoprefixer` to solve this problem.
      typeof window === 'undefined' ? null : jssVendorPrefixer(),
      jssPropsSort(),
    ],
  };
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var objectWithoutPropertiesLoose = _objectWithoutPropertiesLoose;

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

var objectWithoutProperties = _objectWithoutProperties;

function mergeClasses() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var baseClasses = options.baseClasses,
    newClasses = options.newClasses,
    Component = options.Component;

  if (!newClasses) {
    return baseClasses;
  }

  var nextClasses = _extends_1({}, baseClasses);

  Object.keys(newClasses).forEach(function(key) {
    if (newClasses[key]) {
      nextClasses[key] = ''.concat(baseClasses[key], ' ').concat(newClasses[key]);
    }
  });
  return nextClasses;
}

// Used https://github.com/thinkloop/multi-key-cache as inspiration
var multiKeyStore = {
  set: function set(cache, key1, key2, value) {
    var subCache = cache.get(key1);

    if (!subCache) {
      subCache = new Map();
      cache.set(key1, subCache);
    }

    subCache.set(key2, value);
  },
  get: function get(cache, key1, key2) {
    var subCache = cache.get(key1);
    return subCache ? subCache.get(key2) : undefined;
  },
  delete: function _delete(cache, key1, key2) {
    var subCache = cache.get(key1);
    subCache.delete(key2);
  },
};

var ThemeContext = React__default.createContext(null);

function useTheme() {
  return React__default.useContext(ThemeContext);
}

var jss = create(jssPreset()); // Use a singleton or the provided one by the context.
//
// The counter-based approach doesn't tolerate any mistake.
// It's much safer to use the same counter everywhere.

var generateClassName = createGenerateClassName(); // Exported for test purposes

var sheetsManager = new Map();
var defaultOptions = {
  disableGeneration: false,
  generateClassName: generateClassName,
  jss: jss,
  sheetsCache: null,
  sheetsManager: sheetsManager,
  sheetsRegistry: null,
};
var StylesContext = React__default.createContext(defaultOptions);
var injectFirstNode;

function StylesProvider(props) {
  var children = props.children,
    _props$injectFirst = props.injectFirst,
    injectFirst = _props$injectFirst === void 0 ? false : _props$injectFirst,
    _props$disableGenerat = props.disableGeneration,
    disableGeneration = _props$disableGenerat === void 0 ? false : _props$disableGenerat,
    localOptions = objectWithoutProperties(props, ['children', 'injectFirst', 'disableGeneration']);

  var outerOptions = React__default.useContext(StylesContext);

  var context = _extends_1(
    {},
    outerOptions,
    {
      disableGeneration: disableGeneration,
    },
    localOptions,
  );

  if (!context.jss.options.insertionPoint && injectFirst && typeof window !== 'undefined') {
    if (!injectFirstNode) {
      var head = document.head;
      injectFirstNode = document.createComment('mui-inject-first');
      head.insertBefore(injectFirstNode, head.firstChild);
    }

    context.jss = create({
      plugins: jssPreset().plugins,
      insertionPoint: injectFirstNode,
    });
  }

  return React__default.createElement(
    StylesContext.Provider,
    {
      value: context,
    },
    children,
  );
}

/* eslint-disable import/prefer-default-export */
// We create the style sheet during at the creation of the component,
// children are handled after the parents, so the order of style elements would be parent->child.
// It is a problem though when a parent passes a className
// which needs to override any child's styles.
// StyleSheet of the child has a higher specificity, because of the source order.
// So our solution is to render sheets them in the reverse order child->sheet, so
// that parent has a higher specificity.

var indexCounter = -1e9;
function increment() {
  indexCounter += 1;
  return indexCounter;
}

var isMergeableObject = function isMergeableObject(value) {
  return isNonNullObject(value) && !isSpecial(value);
};

function isNonNullObject(value) {
  return !!value && typeof value === 'object';
}

function isSpecial(value) {
  var stringValue = Object.prototype.toString.call(value);

  return (
    stringValue === '[object RegExp]' || stringValue === '[object Date]' || isReactElement(value)
  );
}

// see https://github.com/facebook/react/blob/b5ac963fb791d1298e7f396236383bc955f916c1/src/isomorphic/classic/element/ReactElement.js#L21-L25
var canUseSymbol = typeof Symbol === 'function' && Symbol.for;
var REACT_ELEMENT_TYPE = canUseSymbol ? Symbol.for('react.element') : 0xeac7;

function isReactElement(value) {
  return value.$$typeof === REACT_ELEMENT_TYPE;
}

function emptyTarget(val) {
  return Array.isArray(val) ? [] : {};
}

function cloneUnlessOtherwiseSpecified(value, options) {
  return options.clone !== false && options.isMergeableObject(value)
    ? deepmerge(emptyTarget(value), value, options)
    : value;
}

function defaultArrayMerge(target, source, options) {
  return target.concat(source).map(function(element) {
    return cloneUnlessOtherwiseSpecified(element, options);
  });
}

function getMergeFunction(key, options) {
  if (!options.customMerge) {
    return deepmerge;
  }
  var customMerge = options.customMerge(key);
  return typeof customMerge === 'function' ? customMerge : deepmerge;
}

function getEnumerableOwnPropertySymbols(target) {
  return Object.getOwnPropertySymbols
    ? Object.getOwnPropertySymbols(target).filter(function(symbol) {
        return target.propertyIsEnumerable(symbol);
      })
    : [];
}

function getKeys(target) {
  return Object.keys(target).concat(getEnumerableOwnPropertySymbols(target));
}

function mergeObject(target, source, options) {
  var destination = {};
  if (options.isMergeableObject(target)) {
    getKeys(target).forEach(function(key) {
      destination[key] = cloneUnlessOtherwiseSpecified(target[key], options);
    });
  }
  getKeys(source).forEach(function(key) {
    if (!options.isMergeableObject(source[key]) || !target[key]) {
      destination[key] = cloneUnlessOtherwiseSpecified(source[key], options);
    } else {
      destination[key] = getMergeFunction(key, options)(target[key], source[key], options);
    }
  });
  return destination;
}

function deepmerge(target, source, options) {
  options = options || {};
  options.arrayMerge = options.arrayMerge || defaultArrayMerge;
  options.isMergeableObject = options.isMergeableObject || isMergeableObject;

  var sourceIsArray = Array.isArray(source);
  var targetIsArray = Array.isArray(target);
  var sourceAndTargetTypesMatch = sourceIsArray === targetIsArray;

  if (!sourceAndTargetTypesMatch) {
    return cloneUnlessOtherwiseSpecified(source, options);
  } else if (sourceIsArray) {
    return options.arrayMerge(target, source, options);
  } else {
    return mergeObject(target, source, options);
  }
}

deepmerge.all = function deepmergeAll(array, options) {
  if (!Array.isArray(array)) {
    throw new Error('first argument should be an array');
  }

  return array.reduce(function(prev, next) {
    return deepmerge(prev, next, options);
  }, {});
};

var deepmerge_1 = deepmerge;

var cjs = deepmerge_1;

// We use the same empty object to ref count the styles that don't need a theme object.
var noopTheme = {};

function arrayMerge(destination, source) {
  return source;
}

function getStylesCreator(stylesOrCreator) {
  var themingEnabled = typeof stylesOrCreator === 'function';
  return {
    create: function create(theme, name) {
      var styles;

      try {
        styles = themingEnabled ? stylesOrCreator(theme) : stylesOrCreator;
      } catch (err) {
        throw err;
      }

      if (!name || !theme.overrides || !theme.overrides[name]) {
        return styles;
      }

      var overrides = theme.overrides[name];

      var stylesWithOverrides = _extends_1({}, styles);

      Object.keys(overrides).forEach(function(key) {
        stylesWithOverrides[key] = cjs(stylesWithOverrides[key], overrides[key], {
          arrayMerge: arrayMerge,
        });
      });
      return stylesWithOverrides;
    },
    options: {},
    themingEnabled: themingEnabled,
  };
}

function getClasses(_ref, classes, Component) {
  var state = _ref.state,
    stylesOptions = _ref.stylesOptions;

  if (stylesOptions.disableGeneration) {
    return classes || {};
  }

  if (!state.cacheClasses) {
    state.cacheClasses = {
      // Cache for the finalized classes value.
      value: null,
      // Cache for the last used classes prop pointer.
      lastProp: null,
      // Cache for the last used rendered classes pointer.
      lastJSS: {},
    };
  } // Tracks if either the rendered classes or classes prop has changed,
  // requiring the generation of a new finalized classes object.

  var generate = false;

  if (state.classes !== state.cacheClasses.lastJSS) {
    state.cacheClasses.lastJSS = state.classes;
    generate = true;
  }

  if (classes !== state.cacheClasses.lastProp) {
    state.cacheClasses.lastProp = classes;
    generate = true;
  }

  if (generate) {
    state.cacheClasses.value = mergeClasses({
      baseClasses: state.cacheClasses.lastJSS,
      newClasses: classes,
      Component: Component,
    });
  }

  return state.cacheClasses.value;
}

function attach(_ref2, props) {
  var state = _ref2.state,
    theme = _ref2.theme,
    stylesOptions = _ref2.stylesOptions,
    stylesCreator = _ref2.stylesCreator,
    name = _ref2.name;

  if (stylesOptions.disableGeneration) {
    return;
  }

  var sheetManager = multiKeyStore.get(stylesOptions.sheetsManager, stylesCreator, theme);

  if (!sheetManager) {
    sheetManager = {
      refs: 0,
      staticSheet: null,
      dynamicStyles: null,
    };
    multiKeyStore.set(stylesOptions.sheetsManager, stylesCreator, theme, sheetManager);
  }

  var options = _extends_1({}, stylesCreator.options, stylesOptions, {
    theme: theme,
    flip: typeof stylesOptions.flip === 'boolean' ? stylesOptions.flip : theme.direction === 'rtl',
  });

  options.generateId = options.serverGenerateClassName || options.generateClassName;
  var sheetsRegistry = stylesOptions.sheetsRegistry;

  if (sheetManager.refs === 0) {
    var staticSheet;

    if (stylesOptions.sheetsCache) {
      staticSheet = multiKeyStore.get(stylesOptions.sheetsCache, stylesCreator, theme);
    }

    var styles = stylesCreator.create(theme, name);

    if (!staticSheet) {
      staticSheet = stylesOptions.jss.createStyleSheet(
        styles,
        _extends_1(
          {
            link: false,
          },
          options,
        ),
      );
      staticSheet.attach();

      if (stylesOptions.sheetsCache) {
        multiKeyStore.set(stylesOptions.sheetsCache, stylesCreator, theme, staticSheet);
      }
    }

    if (sheetsRegistry) {
      sheetsRegistry.add(staticSheet);
    }

    sheetManager.staticSheet = staticSheet;
    sheetManager.dynamicStyles = getDynamicStyles(styles);
  }

  if (sheetManager.dynamicStyles) {
    var dynamicSheet = stylesOptions.jss.createStyleSheet(
      sheetManager.dynamicStyles,
      _extends_1(
        {
          link: true,
        },
        options,
      ),
    );
    dynamicSheet.update(props).attach();
    state.dynamicSheet = dynamicSheet;
    state.classes = mergeClasses({
      baseClasses: sheetManager.staticSheet.classes,
      newClasses: dynamicSheet.classes,
    });

    if (sheetsRegistry) {
      sheetsRegistry.add(dynamicSheet);
    }
  } else {
    state.classes = sheetManager.staticSheet.classes;
  }

  sheetManager.refs += 1;
}

function update(_ref3, props) {
  var state = _ref3.state;

  if (state.dynamicSheet) {
    state.dynamicSheet.update(props);
  }
}

function detach(_ref4) {
  var state = _ref4.state,
    theme = _ref4.theme,
    stylesOptions = _ref4.stylesOptions,
    stylesCreator = _ref4.stylesCreator;

  if (stylesOptions.disableGeneration) {
    return;
  }

  var sheetManager = multiKeyStore.get(stylesOptions.sheetsManager, stylesCreator, theme);
  sheetManager.refs -= 1;
  var sheetsRegistry = stylesOptions.sheetsRegistry;

  if (sheetManager.refs === 0) {
    multiKeyStore.delete(stylesOptions.sheetsManager, stylesCreator, theme);
    stylesOptions.jss.removeStyleSheet(sheetManager.staticSheet);

    if (sheetsRegistry) {
      sheetsRegistry.remove(sheetManager.staticSheet);
    }
  }

  if (state.dynamicSheet) {
    stylesOptions.jss.removeStyleSheet(state.dynamicSheet);

    if (sheetsRegistry) {
      sheetsRegistry.remove(state.dynamicSheet);
    }
  }
}

function useSynchronousEffect(func, values) {
  var key = React__default.useRef([]);
  var output; // Store "generation" key. Just returns a new object every time

  var currentKey = React__default.useMemo(function() {
    return {};
  }, values); // eslint-disable-line react-hooks/exhaustive-deps
  // "the first render", or "memo dropped the value"

  if (key.current !== currentKey) {
    key.current = currentKey;
    output = func();
  }

  React__default.useEffect(
    function() {
      return function() {
        if (output) {
          output();
        }
      };
    },
    [currentKey], // eslint-disable-line react-hooks/exhaustive-deps
  );
}

function makeStyles(stylesOrCreator) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var name = options.name,
    classNamePrefixOption = options.classNamePrefix,
    Component = options.Component,
    _options$defaultTheme = options.defaultTheme,
    defaultTheme = _options$defaultTheme === void 0 ? noopTheme : _options$defaultTheme,
    stylesOptions2 = objectWithoutProperties(options, [
      'name',
      'classNamePrefix',
      'Component',
      'defaultTheme',
    ]);

  var stylesCreator = getStylesCreator(stylesOrCreator);
  var classNamePrefix = name || classNamePrefixOption || 'makeStyles';
  stylesCreator.options = {
    index: increment(),
    name: name,
    meta: classNamePrefix,
    classNamePrefix: classNamePrefix,
  };
  var listenToTheme = stylesCreator.themingEnabled || typeof name === 'string';
  return function() {
    var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var theme = (listenToTheme ? useTheme() : null) || defaultTheme;

    var stylesOptions = _extends_1({}, React__default.useContext(StylesContext), stylesOptions2);

    var instance = React__default.useRef();
    var shouldUpdate = React__default.useRef();
    useSynchronousEffect(
      function() {
        var current = {
          name: name,
          state: {},
          stylesCreator: stylesCreator,
          stylesOptions: stylesOptions,
          theme: theme,
        };
        attach(current, props);
        shouldUpdate.current = false;
        instance.current = current;
        return function() {
          detach(current);
        };
      },
      [theme, stylesCreator],
    );
    React__default.useEffect(function() {
      if (shouldUpdate.current) {
        update(instance.current, props);
      }

      shouldUpdate.current = true;
    });
    return getClasses(instance.current, props.classes, Component);
  };
}

function toVal(mix) {
  var k,
    y,
    str = '';
  if (mix) {
    if (typeof mix === 'object') {
      if (!!mix.push) {
        for (k = 0; k < mix.length; k++) {
          if (mix[k] && (y = toVal(mix[k]))) {
            str && (str += ' ');
            str += y;
          }
        }
      } else {
        for (k in mix) {
          if (mix[k] && (y = toVal(k))) {
            str && (str += ' ');
            str += y;
          }
        }
      }
    } else if (typeof mix !== 'boolean' && !mix.call) {
      str && (str += ' ');
      str += mix;
    }
  }
  return str;
}

function clsx() {
  var i = 0,
    x,
    str = '';
  while (i < arguments.length) {
    if ((x = toVal(arguments[i++]))) {
      str && (str += ' ');
      str += x;
    }
  }
  return str;
}

/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */

var REACT_STATICS = {
  childContextTypes: true,
  contextType: true,
  contextTypes: true,
  defaultProps: true,
  displayName: true,
  getDefaultProps: true,
  getDerivedStateFromError: true,
  getDerivedStateFromProps: true,
  mixins: true,
  propTypes: true,
  type: true,
};

var KNOWN_STATICS = {
  name: true,
  length: true,
  prototype: true,
  caller: true,
  callee: true,
  arguments: true,
  arity: true,
};

var FORWARD_REF_STATICS = {
  $$typeof: true,
  render: true,
  defaultProps: true,
  displayName: true,
  propTypes: true,
};

var MEMO_STATICS = {
  $$typeof: true,
  compare: true,
  defaultProps: true,
  displayName: true,
  propTypes: true,
  type: true,
};

var TYPE_STATICS = {};
TYPE_STATICS[reactIs.ForwardRef] = FORWARD_REF_STATICS;

function getStatics(component) {
  if (reactIs.isMemo(component)) {
    return MEMO_STATICS;
  }
  return TYPE_STATICS[component['$$typeof']] || REACT_STATICS;
}

var defineProperty$1 = Object.defineProperty;
var getOwnPropertyNames = Object.getOwnPropertyNames;
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;
var getPrototypeOf = Object.getPrototypeOf;
var objectPrototype = Object.prototype;

function hoistNonReactStatics(targetComponent, sourceComponent, blacklist) {
  if (typeof sourceComponent !== 'string') {
    // don't hoist over string (html) components

    if (objectPrototype) {
      var inheritedComponent = getPrototypeOf(sourceComponent);
      if (inheritedComponent && inheritedComponent !== objectPrototype) {
        hoistNonReactStatics(targetComponent, inheritedComponent, blacklist);
      }
    }

    var keys = getOwnPropertyNames(sourceComponent);

    if (getOwnPropertySymbols) {
      keys = keys.concat(getOwnPropertySymbols(sourceComponent));
    }

    var targetStatics = getStatics(targetComponent);
    var sourceStatics = getStatics(sourceComponent);

    for (var i = 0; i < keys.length; ++i) {
      var key = keys[i];
      if (
        !KNOWN_STATICS[key] &&
        !(blacklist && blacklist[key]) &&
        !(sourceStatics && sourceStatics[key]) &&
        !(targetStatics && targetStatics[key])
      ) {
        var descriptor = getOwnPropertyDescriptor(sourceComponent, key);
        try {
          // Avoid failures from read-only properties
          defineProperty$1(targetComponent, key, descriptor);
        } catch (e) {}
      }
    }

    return targetComponent;
  }

  return targetComponent;
}

var hoistNonReactStatics_cjs = hoistNonReactStatics;

// It does not modify the component passed to it;
// instead, it returns a new component, with a `classes` property.

var withStyles = function withStyles(stylesOrCreator) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return function(Component) {
    var defaultTheme = options.defaultTheme,
      _options$withTheme = options.withTheme,
      withTheme = _options$withTheme === void 0 ? false : _options$withTheme,
      name = options.name,
      stylesOptions = objectWithoutProperties(options, ['defaultTheme', 'withTheme', 'name']);

    var classNamePrefix = name;

    var useStyles = makeStyles(
      stylesOrCreator,
      _extends_1(
        {
          defaultTheme: defaultTheme,
          Component: Component,
          name: name || Component.displayName,
          classNamePrefix: classNamePrefix,
        },
        stylesOptions,
      ),
    );
    var WithStyles = React__default.forwardRef(function WithStyles(props, ref) {
      var classesProp = props.classes,
        innerRef = props.innerRef,
        other = objectWithoutProperties(props, ['classes', 'innerRef']);

      var classes = useStyles(props);
      var theme;
      var more = other;

      if (typeof name === 'string' || withTheme) {
        // name and withTheme are invariant in the outer scope
        // eslint-disable-next-line react-hooks/rules-of-hooks
        theme = useTheme() || defaultTheme;

        if (name) {
          more = getThemeProps({
            theme: theme,
            name: name,
            props: other,
          });
        } // Provide the theme to the wrapped component.
        // So we don't have to use the `withTheme()` Higher-order Component.

        if (withTheme && !more.theme) {
          more.theme = theme;
        }
      }

      return React__default.createElement(
        Component,
        _extends_1(
          {
            ref: innerRef || ref,
            classes: classes,
          },
          more,
        ),
      );
    });

    hoistNonReactStatics_cjs(WithStyles, Component);

    return WithStyles;
  };
};

var ACTIONS = {
  SET_ROWS: 'SET_ROWS',
  SET_PAGE: 'SET_PAGE',
  SET_ORDER: 'SET_ORDER',
  SET_ORDER_BY: 'SET_ORDER_BY',
  SET_ROWS_PER_PAGE: 'SET_ROWS_PER_PAGE',
};
function reducer(state, action) {
  switch (action.type) {
    case ACTIONS.SET_ROWS:
      return _objectSpread2({}, state, {
        rows: _toConsumableArray(action.data.rows),
      });

    case ACTIONS.SET_ROWS_PER_PAGE:
      return _objectSpread2({}, state, {
        rowsPerPage: action.data.rowsPerPage,
      });

    case ACTIONS.SET_PAGE:
      return _objectSpread2({}, state, {
        page: action.data.page,
      });

    case ACTIONS.SET_ORDER:
      return _objectSpread2({}, state, {
        order: action.data.order,
      });

    case ACTIONS.SET_ORDER_BY:
      return _objectSpread2({}, state, {
        orderBy: action.data.orderBy,
      });

    default:
      return state;
  }
}

var setRows = function setRows(dispatch) {
  return function(rows) {
    dispatch({
      type: ACTIONS.SET_ROWS,
      data: {
        rows: rows,
      },
    });
  };
};

var setOrder = function setOrder(dispatch) {
  return function(order) {
    dispatch({
      type: ACTIONS.SET_ORDER,
      data: {
        order: order,
      },
    });
  };
};

var setOrderBy = function setOrderBy(dispatch) {
  return function(orderBy) {
    dispatch({
      type: ACTIONS.SET_ORDER_BY,
      data: {
        orderBy: orderBy,
      },
    });
  };
};

var setPage = function setPage(dispatch) {
  return function(page) {
    dispatch({
      type: ACTIONS.SET_PAGE,
      data: {
        page: page,
      },
    });
  };
};

var setRowsPerPage = function setRowsPerPage(dispatch) {
  return function(rowsPerPage) {
    dispatch({
      type: ACTIONS.SET_ROWS_PER_PAGE,
      data: {
        rowsPerPage: rowsPerPage,
      },
    });
  };
};

var DataTableContainer = function DataTableContainer(_ref) {
  var _ref$store = _ref.store,
    store =
      _ref$store === void 0
        ? {
            load: function load() {},
          }
        : _ref$store,
    _ref$rowsPerPage = _ref.rowsPerPage,
    rowsPerPage = _ref$rowsPerPage === void 0 ? 10 : _ref$rowsPerPage,
    _ref$order = _ref.order,
    order = _ref$order === void 0 ? 'asc' : _ref$order,
    _ref$orderBy = _ref.orderBy,
    orderBy = _ref$orderBy === void 0 ? '' : _ref$orderBy,
    _ref$page = _ref.page,
    page = _ref$page === void 0 ? 0 : _ref$page,
    children = _ref.children;

  var _useReducer = React.useReducer(reducer, {
      rows: [],
      page: page,
      rowsPerPage: rowsPerPage,
      order: order,
      orderBy: orderBy,
    }),
    _useReducer2 = _slicedToArray(_useReducer, 2),
    tableState = _useReducer2[0],
    dispatch = _useReducer2[1];

  React.useEffect(
    function() {
      store.load(setRows(dispatch));
    },
    [store],
  );

  var _children$filter = children.filter(function(child) {
      return child.type && child.type.displayName === 'DataTableHead';
    }),
    _children$filter2 = _slicedToArray(_children$filter, 1),
    tableHeader = _children$filter2[0];

  var cols = tableHeader.props.children.map(function(_ref2) {
    var _ref2$props = _ref2.props,
      index = _ref2$props.index,
      type = _ref2$props.type,
      disablePadding = _ref2$props.disablePadding,
      label = _ref2$props.label,
      render = _ref2$props.render;
    return {
      index: index,
      type: type,
      disablePadding: disablePadding,
      label: label,
      render: render,
    };
  });

  var _children$filter3 = children.filter(function(child) {
      return child.type && child.type.displayName === 'DataTableBody';
    }),
    _children$filter4 = _slicedToArray(_children$filter3, 1),
    tableBody = _children$filter4[0];

  return _objectSpread2({}, tableState, {
    cols: cols,
    isTableBody: !!tableBody,
    setRows: setRows(dispatch),
    setOrder: setOrder(dispatch),
    setOrderBy: setOrderBy(dispatch),
    setPage: setPage(dispatch),
    setRowsPerPage: setRowsPerPage(dispatch),
  });
};

/* eslint-disable no-use-before-define */
/**
 * Returns a number whose value is limited to the given range.
 *
 * @param {number} value The value to be clamped
 * @param {number} min The lower boundary of the output range
 * @param {number} max The upper boundary of the output range
 * @returns {number} A number in the range [min, max]
 */

function clamp(value) {
  var min = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var max = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

  if (value < min) {
    return min;
  }

  if (value > max) {
    return max;
  }

  return value;
}
/**
 * Converts a color from CSS hex format to CSS rgb format.
 *
 * @param {string} color - Hex color, i.e. #nnn or #nnnnnn
 * @returns {string} A CSS rgb color string
 */

function hexToRgb(color) {
  color = color.substr(1);
  var re = new RegExp('.{1,'.concat(color.length / 3, '}'), 'g');
  var colors = color.match(re);

  if (colors && colors[0].length === 1) {
    colors = colors.map(function(n) {
      return n + n;
    });
  }

  return colors
    ? 'rgb('.concat(
        colors
          .map(function(n) {
            return parseInt(n, 16);
          })
          .join(', '),
        ')',
      )
    : '';
}
/**
 * Converts a color from hsl format to rgb format.
 *
 * @param {string} color - HSL color values
 * @returns {string} rgb color values
 */

function hslToRgb(color) {
  color = decomposeColor(color);
  var _color = color,
    values = _color.values;
  var h = values[0];
  var s = values[1] / 100;
  var l = values[2] / 100;
  var a = s * Math.min(l, 1 - l);

  var f = function f(n) {
    var k = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : (n + h / 30) % 12;
    return l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
  };

  var type = 'rgb';
  var rgb = [Math.round(f(0) * 255), Math.round(f(8) * 255), Math.round(f(4) * 255)];

  if (color.type === 'hsla') {
    type += 'a';
    rgb.push(values[3]);
  }

  return recomposeColor({
    type: type,
    values: rgb,
  });
}
/**
 * Returns an object with the type and values of a color.
 *
 * Note: Does not support rgb % values.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @returns {object} - A MUI color object: {type: string, values: number[]}
 */

function decomposeColor(color) {
  // Idempotent
  if (color.type) {
    return color;
  }

  if (color.charAt(0) === '#') {
    return decomposeColor(hexToRgb(color));
  }

  var marker = color.indexOf('(');
  var type = color.substring(0, marker);

  if (['rgb', 'rgba', 'hsl', 'hsla'].indexOf(type) === -1) {
    throw new Error(
      [
        'Material-UI: unsupported `'.concat(color, '` color.'),
        'We support the following formats: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla().',
      ].join('\n'),
    );
  }

  var values = color.substring(marker + 1, color.length - 1).split(',');
  values = values.map(function(value) {
    return parseFloat(value);
  });
  return {
    type: type,
    values: values,
  };
}
/**
 * Converts a color object with type and values to a string.
 *
 * @param {object} color - Decomposed color
 * @param {string} color.type - One of: 'rgb', 'rgba', 'hsl', 'hsla'
 * @param {array} color.values - [n,n,n] or [n,n,n,n]
 * @returns {string} A CSS color string
 */

function recomposeColor(color) {
  var type = color.type;
  var values = color.values;

  if (type.indexOf('rgb') !== -1) {
    // Only convert the first 3 values to int (i.e. not alpha)
    values = values.map(function(n, i) {
      return i < 3 ? parseInt(n, 10) : n;
    });
  } else if (type.indexOf('hsl') !== -1) {
    values[1] = ''.concat(values[1], '%');
    values[2] = ''.concat(values[2], '%');
  }

  return ''.concat(type, '(').concat(values.join(', '), ')');
}
/**
 * Calculates the contrast ratio between two colors.
 *
 * Formula: https://www.w3.org/TR/WCAG20-TECHS/G17.html#G17-tests
 *
 * @param {string} foreground - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {string} background - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @returns {number} A contrast ratio value in the range 0 - 21.
 */

function getContrastRatio(foreground, background) {
  var lumA = getLuminance(foreground);
  var lumB = getLuminance(background);
  return (Math.max(lumA, lumB) + 0.05) / (Math.min(lumA, lumB) + 0.05);
}
/**
 * The relative brightness of any point in a color space,
 * normalized to 0 for darkest black and 1 for lightest white.
 *
 * Formula: https://www.w3.org/TR/WCAG20-TECHS/G17.html#G17-tests
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @returns {number} The relative brightness of the color in the range 0 - 1
 */

function getLuminance(color) {
  color = decomposeColor(color);
  var rgb = color.type === 'hsl' ? decomposeColor(hslToRgb(color)).values : color.values;
  rgb = rgb.map(function(val) {
    val /= 255; // normalized

    return val <= 0.03928 ? val / 12.92 : Math.pow((val + 0.055) / 1.055, 2.4);
  }); // Truncate at 3 digits

  return Number((0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]).toFixed(3));
}
/**
 * Set the absolute transparency of a color.
 * Any existing alpha values are overwritten.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} value - value to set the alpha channel to in the range 0 -1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */

function fade(color, value) {
  color = decomposeColor(color);
  value = clamp(value);

  if (color.type === 'rgb' || color.type === 'hsl') {
    color.type += 'a';
  }

  color.values[3] = value;
  return recomposeColor(color);
}
/**
 * Darkens a color.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} coefficient - multiplier in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */

function darken(color, coefficient) {
  color = decomposeColor(color);
  coefficient = clamp(coefficient);

  if (color.type.indexOf('hsl') !== -1) {
    color.values[2] *= 1 - coefficient;
  } else if (color.type.indexOf('rgb') !== -1) {
    for (var i = 0; i < 3; i += 1) {
      color.values[i] *= 1 - coefficient;
    }
  }

  return recomposeColor(color);
}
/**
 * Lightens a color.
 *
 * @param {string} color - CSS color, i.e. one of: #nnn, #nnnnnn, rgb(), rgba(), hsl(), hsla()
 * @param {number} coefficient - multiplier in the range 0 - 1
 * @returns {string} A CSS color string. Hex input values are returned as rgb
 */

function lighten(color, coefficient) {
  color = decomposeColor(color);
  coefficient = clamp(coefficient);

  if (color.type.indexOf('hsl') !== -1) {
    color.values[2] += (100 - color.values[2]) * coefficient;
  } else if (color.type.indexOf('rgb') !== -1) {
    for (var i = 0; i < 3; i += 1) {
      color.values[i] += (255 - color.values[i]) * coefficient;
    }
  }

  return recomposeColor(color);
}

/*!
 * isobject <https://github.com/jonschlinkert/isobject>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */

function isObject(val) {
  return val != null && typeof val === 'object' && Array.isArray(val) === false;
}

/*!
 * is-plain-object <https://github.com/jonschlinkert/is-plain-object>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */

function isObjectObject(o) {
  return isObject(o) === true && Object.prototype.toString.call(o) === '[object Object]';
}

function isPlainObject(o) {
  var ctor, prot;

  if (isObjectObject(o) === false) return false;

  // If has modified constructor
  ctor = o.constructor;
  if (typeof ctor !== 'function') return false;

  // If has modified prototype
  prot = ctor.prototype;
  if (isObjectObject(prot) === false) return false;

  // If constructor does not have an Object-specific method
  if (prot.hasOwnProperty('isPrototypeOf') === false) {
    return false;
  }

  // Most likely a plain Object
  return true;
}

// Sorted ASC by size. That's important.
// It can't be configured as it's used statically for propTypes.
var keys = ['xs', 'sm', 'md', 'lg', 'xl']; // Keep in mind that @media is inclusive by the CSS specification.

function createBreakpoints(breakpoints) {
  var _breakpoints$values = breakpoints.values,
    values =
      _breakpoints$values === void 0
        ? {
            xs: 0,
            sm: 600,
            md: 960,
            lg: 1280,
            xl: 1920,
          }
        : _breakpoints$values,
    _breakpoints$unit = breakpoints.unit,
    unit = _breakpoints$unit === void 0 ? 'px' : _breakpoints$unit,
    _breakpoints$step = breakpoints.step,
    step = _breakpoints$step === void 0 ? 5 : _breakpoints$step,
    other = objectWithoutProperties(breakpoints, ['values', 'unit', 'step']);

  function up(key) {
    var value = typeof values[key] === 'number' ? values[key] : key;
    return '@media (min-width:'.concat(value).concat(unit, ')');
  }

  function down(key) {
    var endIndex = keys.indexOf(key) + 1;
    var upperbound = values[keys[endIndex]];

    if (endIndex === keys.length) {
      // xl down applies to all sizes
      return up('xs');
    }

    var value = typeof upperbound === 'number' && endIndex > 0 ? upperbound : key;
    return '@media (max-width:'.concat(value - step / 100).concat(unit, ')');
  }

  function between(start, end) {
    var endIndex = keys.indexOf(end) + 1;

    if (endIndex === keys.length) {
      return up(start);
    }

    return (
      '@media (min-width:'.concat(values[start]).concat(unit, ') and ') +
      '(max-width:'.concat(values[keys[endIndex]] - step / 100).concat(unit, ')')
    );
  }

  function only(key) {
    return between(key, key);
  }

  function width(key) {
    return values[key];
  }

  return _extends_1(
    {
      keys: keys,
      values: values,
      up: up,
      down: down,
      between: between,
      only: only,
      width: width,
    },
    other,
  );
}

// import warning from 'warning';
function createMixins(breakpoints, spacing, mixins) {
  var _toolbar;

  return _extends_1(
    {
      gutters: function gutters() {
        var styles = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        // To deprecate in v4.1
        //       warning(
        //         false,
        //         [
        //           'Material-UI: theme.mixins.gutters() is deprecated.',
        //           'You can use the source of the mixin directly:',
        //           `
        // paddingLeft: theme.spacing(2),
        // paddingRight: theme.spacing(2),
        // [theme.breakpoints.up('sm')]: {
        //   paddingLeft: theme.spacing(3),
        //   paddingRight: theme.spacing(3),
        // },
        // `,
        //         ].join('\n'),
        //       );
        return _extends_1(
          {
            paddingLeft: spacing(2),
            paddingRight: spacing(2),
          },
          styles,
          defineProperty(
            {},
            breakpoints.up('sm'),
            _extends_1(
              {
                paddingLeft: spacing(3),
                paddingRight: spacing(3),
              },
              styles[breakpoints.up('sm')],
            ),
          ),
        );
      },
      toolbar:
        ((_toolbar = {
          minHeight: 56,
        }),
        defineProperty(_toolbar, ''.concat(breakpoints.up('xs'), ' and (orientation: landscape)'), {
          minHeight: 48,
        }),
        defineProperty(_toolbar, breakpoints.up('sm'), {
          minHeight: 64,
        }),
        _toolbar),
    },
    mixins,
  );
}

var indigo = {
  50: '#e8eaf6',
  100: '#c5cae9',
  200: '#9fa8da',
  300: '#7986cb',
  400: '#5c6bc0',
  500: '#3f51b5',
  600: '#3949ab',
  700: '#303f9f',
  800: '#283593',
  900: '#1a237e',
  A100: '#8c9eff',
  A200: '#536dfe',
  A400: '#3d5afe',
  A700: '#304ffe',
};

var pink = {
  50: '#fce4ec',
  100: '#f8bbd0',
  200: '#f48fb1',
  300: '#f06292',
  400: '#ec407a',
  500: '#e91e63',
  600: '#d81b60',
  700: '#c2185b',
  800: '#ad1457',
  900: '#880e4f',
  A100: '#ff80ab',
  A200: '#ff4081',
  A400: '#f50057',
  A700: '#c51162',
};

var grey = {
  50: '#fafafa',
  100: '#f5f5f5',
  200: '#eeeeee',
  300: '#e0e0e0',
  400: '#bdbdbd',
  500: '#9e9e9e',
  600: '#757575',
  700: '#616161',
  800: '#424242',
  900: '#212121',
  A100: '#d5d5d5',
  A200: '#aaaaaa',
  A400: '#303030',
  A700: '#616161',
};

var red = {
  50: '#ffebee',
  100: '#ffcdd2',
  200: '#ef9a9a',
  300: '#e57373',
  400: '#ef5350',
  500: '#f44336',
  600: '#e53935',
  700: '#d32f2f',
  800: '#c62828',
  900: '#b71c1c',
  A100: '#ff8a80',
  A200: '#ff5252',
  A400: '#ff1744',
  A700: '#d50000',
};

var common = {
  black: '#000',
  white: '#fff',
};

var light = {
  // The colors used to style the text.
  text: {
    // The most important text.
    primary: 'rgba(0, 0, 0, 0.87)',
    // Secondary text.
    secondary: 'rgba(0, 0, 0, 0.54)',
    // Disabled text have even lower visual prominence.
    disabled: 'rgba(0, 0, 0, 0.38)',
    // Text hints.
    hint: 'rgba(0, 0, 0, 0.38)',
  },
  // The color used to divide different elements.
  divider: 'rgba(0, 0, 0, 0.12)',
  // The background colors used to style the surfaces.
  // Consistency between these values is important.
  background: {
    paper: common.white,
    default: grey[50],
  },
  // The colors used to style the action elements.
  action: {
    // The color of an active action like an icon button.
    active: 'rgba(0, 0, 0, 0.54)',
    // The color of an hovered action.
    hover: 'rgba(0, 0, 0, 0.08)',
    hoverOpacity: 0.08,
    // The color of a selected action.
    selected: 'rgba(0, 0, 0, 0.14)',
    // The color of a disabled action.
    disabled: 'rgba(0, 0, 0, 0.26)',
    // The background color of a disabled action.
    disabledBackground: 'rgba(0, 0, 0, 0.12)',
  },
};
var dark = {
  text: {
    primary: common.white,
    secondary: 'rgba(255, 255, 255, 0.7)',
    disabled: 'rgba(255, 255, 255, 0.5)',
    hint: 'rgba(255, 255, 255, 0.5)',
    icon: 'rgba(255, 255, 255, 0.5)',
  },
  divider: 'rgba(255, 255, 255, 0.12)',
  background: {
    paper: grey[800],
    default: '#303030',
  },
  action: {
    active: common.white,
    hover: 'rgba(255, 255, 255, 0.1)',
    hoverOpacity: 0.1,
    selected: 'rgba(255, 255, 255, 0.2)',
    disabled: 'rgba(255, 255, 255, 0.3)',
    disabledBackground: 'rgba(255, 255, 255, 0.12)',
  },
};

function addLightOrDark(intent, direction, shade, tonalOffset) {
  if (!intent[direction]) {
    if (intent.hasOwnProperty(shade)) {
      intent[direction] = intent[shade];
    } else if (direction === 'light') {
      intent.light = lighten(intent.main, tonalOffset);
    } else if (direction === 'dark') {
      intent.dark = darken(intent.main, tonalOffset * 1.5);
    }
  }
}

function createPalette(palette) {
  var _palette$primary = palette.primary,
    primary =
      _palette$primary === void 0
        ? {
            light: indigo[300],
            main: indigo[500],
            dark: indigo[700],
          }
        : _palette$primary,
    _palette$secondary = palette.secondary,
    secondary =
      _palette$secondary === void 0
        ? {
            light: pink.A200,
            main: pink.A400,
            dark: pink.A700,
          }
        : _palette$secondary,
    _palette$error = palette.error,
    error =
      _palette$error === void 0
        ? {
            light: red[300],
            main: red[500],
            dark: red[700],
          }
        : _palette$error,
    _palette$type = palette.type,
    type = _palette$type === void 0 ? 'light' : _palette$type,
    _palette$contrastThre = palette.contrastThreshold,
    contrastThreshold = _palette$contrastThre === void 0 ? 3 : _palette$contrastThre,
    _palette$tonalOffset = palette.tonalOffset,
    tonalOffset = _palette$tonalOffset === void 0 ? 0.2 : _palette$tonalOffset,
    other = objectWithoutProperties(palette, [
      'primary',
      'secondary',
      'error',
      'type',
      'contrastThreshold',
      'tonalOffset',
    ]); // Use the same logic as
  // Bootstrap: https://github.com/twbs/bootstrap/blob/1d6e3710dd447de1a200f29e8fa521f8a0908f70/scss/_functions.scss#L59
  // and material-components-web https://github.com/material-components/material-components-web/blob/ac46b8863c4dab9fc22c4c662dc6bd1b65dd652f/packages/mdc-theme/_functions.scss#L54

  function getContrastText(background) {
    var contrastText =
      getContrastRatio(background, dark.text.primary) >= contrastThreshold
        ? dark.text.primary
        : light.text.primary;

    return contrastText;
  }

  function augmentColor(color) {
    var mainShade = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
    var lightShade = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 300;
    var darkShade = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 700;
    color = _extends_1({}, color);

    if (!color.main && color[mainShade]) {
      color.main = color[mainShade];
    }

    addLightOrDark(color, 'light', lightShade, tonalOffset);
    addLightOrDark(color, 'dark', darkShade, tonalOffset);

    if (!color.contrastText) {
      color.contrastText = getContrastText(color.main);
    }

    return color;
  }

  var types = {
    dark: dark,
    light: light,
  };
  var paletteOutput = cjs(
    _extends_1(
      {
        // A collection of common colors.
        common: common,
        // The palette type, can be light or dark.
        type: type,
        // The colors used to represent primary interface elements for a user.
        primary: augmentColor(primary),
        // The colors used to represent secondary interface elements for a user.
        secondary: augmentColor(secondary, 'A400', 'A200', 'A700'),
        // The colors used to represent interface elements that the user should be made aware of.
        error: augmentColor(error),
        // The grey colors.
        grey: grey,
        // Used by `getContrastText()` to maximize the contrast between the background and
        // the text.
        contrastThreshold: contrastThreshold,
        // Take a background color and return the color of the text to maximize the contrast.
        getContrastText: getContrastText,
        // Generate a rich color object.
        augmentColor: augmentColor,
        // Used by the functions below to shift a color's luminance by approximately
        // two indexes within its tonal palette.
        // E.g., shift from Red 500 to Red 300 or Red 700.
        tonalOffset: tonalOffset,
      },
      types[type],
    ),
    other,
    {
      clone: false, // No need to clone deep
    },
  );
  return paletteOutput;
}

function round(value) {
  return Math.round(value * 1e5) / 1e5;
}

var caseAllCaps = {
  textTransform: 'uppercase',
};
var defaultFontFamily = '"Roboto", "Helvetica", "Arial", sans-serif';
/**
 * @see @link{https://material.io/design/typography/the-type-system.html}
 * @see @link{https://material.io/design/typography/understanding-typography.html}
 */

function createTypography(palette, typography) {
  var _ref = typeof typography === 'function' ? typography(palette) : typography,
    _ref$fontFamily = _ref.fontFamily,
    fontFamily = _ref$fontFamily === void 0 ? defaultFontFamily : _ref$fontFamily,
    _ref$fontSize = _ref.fontSize,
    fontSize = _ref$fontSize === void 0 ? 14 : _ref$fontSize,
    _ref$fontWeightLight = _ref.fontWeightLight,
    fontWeightLight = _ref$fontWeightLight === void 0 ? 300 : _ref$fontWeightLight,
    _ref$fontWeightRegula = _ref.fontWeightRegular,
    fontWeightRegular = _ref$fontWeightRegula === void 0 ? 400 : _ref$fontWeightRegula,
    _ref$fontWeightMedium = _ref.fontWeightMedium,
    fontWeightMedium = _ref$fontWeightMedium === void 0 ? 500 : _ref$fontWeightMedium,
    _ref$fontWeightBold = _ref.fontWeightBold,
    fontWeightBold = _ref$fontWeightBold === void 0 ? 700 : _ref$fontWeightBold,
    _ref$htmlFontSize = _ref.htmlFontSize,
    htmlFontSize = _ref$htmlFontSize === void 0 ? 16 : _ref$htmlFontSize,
    allVariants = _ref.allVariants,
    other = objectWithoutProperties(_ref, [
      'fontFamily',
      'fontSize',
      'fontWeightLight',
      'fontWeightRegular',
      'fontWeightMedium',
      'fontWeightBold',
      'htmlFontSize',
      'allVariants',
    ]);
  var coef = fontSize / 14;

  var pxToRem = function pxToRem(size) {
    return ''.concat((size / htmlFontSize) * coef, 'rem');
  };

  var buildVariant = function buildVariant(fontWeight, size, lineHeight, letterSpacing, casing) {
    return _extends_1(
      {
        fontFamily: fontFamily,
        fontWeight: fontWeight,
        fontSize: pxToRem(size),
        // Unitless following https://meyerweb.com/eric/thoughts/2006/02/08/unitless-line-heights/
        lineHeight: lineHeight,
      },
      fontFamily === defaultFontFamily
        ? {
            letterSpacing: ''.concat(round(letterSpacing / size), 'em'),
          }
        : {},
      casing,
      allVariants,
    );
  };

  var variants = {
    h1: buildVariant(fontWeightLight, 96, 1, -1.5),
    h2: buildVariant(fontWeightLight, 60, 1, -0.5),
    h3: buildVariant(fontWeightRegular, 48, 1.04, 0),
    h4: buildVariant(fontWeightRegular, 34, 1.17, 0.25),
    h5: buildVariant(fontWeightRegular, 24, 1.33, 0),
    h6: buildVariant(fontWeightMedium, 20, 1.6, 0.15),
    subtitle1: buildVariant(fontWeightRegular, 16, 1.75, 0.15),
    subtitle2: buildVariant(fontWeightMedium, 14, 1.57, 0.1),
    body1: buildVariant(fontWeightRegular, 16, 1.5, 0.15),
    body2: buildVariant(fontWeightRegular, 14, 1.43, 0.15),
    button: buildVariant(fontWeightMedium, 14, 1.75, 0.4, caseAllCaps),
    caption: buildVariant(fontWeightRegular, 12, 1.66, 0.4),
    overline: buildVariant(fontWeightRegular, 12, 2.66, 1, caseAllCaps),
  };
  return cjs(
    _extends_1(
      {
        htmlFontSize: htmlFontSize,
        pxToRem: pxToRem,
        round: round,
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontWeightLight: fontWeightLight,
        fontWeightRegular: fontWeightRegular,
        fontWeightMedium: fontWeightMedium,
        fontWeightBold: fontWeightBold,
      },
      variants,
    ),
    other,
    {
      clone: false, // No need to clone deep
    },
  );
}

var shadowKeyUmbraOpacity = 0.2;
var shadowKeyPenumbraOpacity = 0.14;
var shadowAmbientShadowOpacity = 0.12;

function createShadow() {
  return [
    ''
      .concat(arguments.length <= 0 ? undefined : arguments[0], 'px ')
      .concat(arguments.length <= 1 ? undefined : arguments[1], 'px ')
      .concat(arguments.length <= 2 ? undefined : arguments[2], 'px ')
      .concat(arguments.length <= 3 ? undefined : arguments[3], 'px rgba(0,0,0,')
      .concat(shadowKeyUmbraOpacity, ')'),
    ''
      .concat(arguments.length <= 4 ? undefined : arguments[4], 'px ')
      .concat(arguments.length <= 5 ? undefined : arguments[5], 'px ')
      .concat(arguments.length <= 6 ? undefined : arguments[6], 'px ')
      .concat(arguments.length <= 7 ? undefined : arguments[7], 'px rgba(0,0,0,')
      .concat(shadowKeyPenumbraOpacity, ')'),
    ''
      .concat(arguments.length <= 8 ? undefined : arguments[8], 'px ')
      .concat(arguments.length <= 9 ? undefined : arguments[9], 'px ')
      .concat(arguments.length <= 10 ? undefined : arguments[10], 'px ')
      .concat(arguments.length <= 11 ? undefined : arguments[11], 'px rgba(0,0,0,')
      .concat(shadowAmbientShadowOpacity, ')'),
  ].join(',');
}

var shadows = [
  'none',
  createShadow(0, 1, 3, 0, 0, 1, 1, 0, 0, 2, 1, -1),
  createShadow(0, 1, 5, 0, 0, 2, 2, 0, 0, 3, 1, -2),
  createShadow(0, 1, 8, 0, 0, 3, 4, 0, 0, 3, 3, -2),
  createShadow(0, 2, 4, -1, 0, 4, 5, 0, 0, 1, 10, 0),
  createShadow(0, 3, 5, -1, 0, 5, 8, 0, 0, 1, 14, 0),
  createShadow(0, 3, 5, -1, 0, 6, 10, 0, 0, 1, 18, 0),
  createShadow(0, 4, 5, -2, 0, 7, 10, 1, 0, 2, 16, 1),
  createShadow(0, 5, 5, -3, 0, 8, 10, 1, 0, 3, 14, 2),
  createShadow(0, 5, 6, -3, 0, 9, 12, 1, 0, 3, 16, 2),
  createShadow(0, 6, 6, -3, 0, 10, 14, 1, 0, 4, 18, 3),
  createShadow(0, 6, 7, -4, 0, 11, 15, 1, 0, 4, 20, 3),
  createShadow(0, 7, 8, -4, 0, 12, 17, 2, 0, 5, 22, 4),
  createShadow(0, 7, 8, -4, 0, 13, 19, 2, 0, 5, 24, 4),
  createShadow(0, 7, 9, -4, 0, 14, 21, 2, 0, 5, 26, 4),
  createShadow(0, 8, 9, -5, 0, 15, 22, 2, 0, 6, 28, 5),
  createShadow(0, 8, 10, -5, 0, 16, 24, 2, 0, 6, 30, 5),
  createShadow(0, 8, 11, -5, 0, 17, 26, 2, 0, 6, 32, 5),
  createShadow(0, 9, 11, -5, 0, 18, 28, 2, 0, 7, 34, 6),
  createShadow(0, 9, 12, -6, 0, 19, 29, 2, 0, 7, 36, 6),
  createShadow(0, 10, 13, -6, 0, 20, 31, 3, 0, 8, 38, 7),
  createShadow(0, 10, 13, -6, 0, 21, 33, 3, 0, 8, 40, 7),
  createShadow(0, 10, 14, -6, 0, 22, 35, 3, 0, 8, 42, 7),
  createShadow(0, 11, 14, -7, 0, 23, 36, 3, 0, 9, 44, 8),
  createShadow(0, 11, 15, -7, 0, 24, 38, 3, 0, 9, 46, 8),
];

var shape = {
  borderRadius: 4,
};

function createSpacing() {
  var spacingInput = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 8;

  // Already transformed.
  if (spacingInput.mui) {
    return spacingInput;
  } // All components align to an 8dp square baseline grid for mobile, tablet, and desktop.
  // https://material.io/design/layout/understanding-layout.html#pixel-density

  var transform;

  if (typeof spacingInput === 'function') {
    transform = spacingInput;
  } else {
    transform = function transform(factor) {
      return spacingInput * factor;
    };
  }

  var spacing = function spacing() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    if (args.length === 0) {
      return transform(1);
    }

    if (args.length === 1) {
      return transform(args[0]);
    }

    return args
      .map(function(factor) {
        var output = transform(factor);
        return typeof output === 'number' ? ''.concat(output, 'px') : output;
      })
      .join(' ');
  }; // Backward compatibility, to remove in v5.

  Object.defineProperty(spacing, 'unit', {
    get: function get() {
      return spacingInput;
    },
  });
  spacing.mui = true;
  return spacing;
}

// to learn the context in which each easing should be used.

var easing = {
  // This is the most common easing curve.
  easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)',
  // Objects enter the screen at full velocity from off-screen and
  // slowly decelerate to a resting point.
  easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)',
  // Objects leave the screen at full velocity. They do not decelerate when off-screen.
  easeIn: 'cubic-bezier(0.4, 0, 1, 1)',
  // The sharp curve is used by objects that may return to the screen at any time.
  sharp: 'cubic-bezier(0.4, 0, 0.6, 1)',
}; // Follow https://material.io/guidelines/motion/duration-easing.html#duration-easing-common-durations
// to learn when use what timing

var duration = {
  shortest: 150,
  shorter: 200,
  short: 250,
  // most basic recommended timing
  standard: 300,
  // this is to be used in complex animations
  complex: 375,
  // recommended when something is entering screen
  enteringScreen: 225,
  // recommended when something is leaving screen
  leavingScreen: 195,
};
var formatMs = function formatMs(milliseconds) {
  return ''.concat(Math.round(milliseconds), 'ms');
};
/**
 * @param {string|Array} props
 * @param {object} param
 * @param {string} param.prop
 * @param {number} param.duration
 * @param {string} param.easing
 * @param {number} param.delay
 */

var transitions = {
  easing: easing,
  duration: duration,
  create: function create() {
    var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ['all'];
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var _options$duration = options.duration,
      durationOption = _options$duration === void 0 ? duration.standard : _options$duration,
      _options$easing = options.easing,
      easingOption = _options$easing === void 0 ? easing.easeInOut : _options$easing,
      _options$delay = options.delay,
      delay = _options$delay === void 0 ? 0 : _options$delay,
      other = objectWithoutProperties(options, ['duration', 'easing', 'delay']);
    return (Array.isArray(props) ? props : [props])
      .map(function(animatedProp) {
        return ''
          .concat(animatedProp, ' ')
          .concat(
            typeof durationOption === 'string' ? durationOption : formatMs(durationOption),
            ' ',
          )
          .concat(easingOption, ' ')
          .concat(typeof delay === 'string' ? delay : formatMs(delay));
      })
      .join(',');
  },
  getAutoHeightDuration: function getAutoHeightDuration(height) {
    if (!height) {
      return 0;
    }

    var constant = height / 36; // https://www.wolframalpha.com/input/?i=(4+%2B+15+*+(x+%2F+36+)+**+0.25+%2B+(x+%2F+36)+%2F+5)+*+10

    return Math.round((4 + 15 * Math.pow(constant, 0.25) + constant / 5) * 10);
  },
};

// We need to centralize the zIndex definitions as they work
// like global values in the browser.
var zIndex = {
  mobileStepper: 1000,
  appBar: 1100,
  drawer: 1200,
  modal: 1300,
  snackbar: 1400,
  tooltip: 1500,
};

function createMuiTheme() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  var _options$breakpoints = options.breakpoints,
    breakpointsInput = _options$breakpoints === void 0 ? {} : _options$breakpoints,
    _options$mixins = options.mixins,
    mixinsInput = _options$mixins === void 0 ? {} : _options$mixins,
    _options$palette = options.palette,
    paletteInput = _options$palette === void 0 ? {} : _options$palette,
    shadowsInput = options.shadows,
    spacingInput = options.spacing,
    _options$typography = options.typography,
    typographyInput = _options$typography === void 0 ? {} : _options$typography,
    other = objectWithoutProperties(options, [
      'breakpoints',
      'mixins',
      'palette',
      'shadows',
      'spacing',
      'typography',
    ]);

  var palette = createPalette(paletteInput);
  var breakpoints = createBreakpoints(breakpointsInput);
  var spacing = createSpacing(spacingInput);

  var muiTheme = _extends_1(
    {
      breakpoints: breakpoints,
      direction: 'ltr',
      mixins: createMixins(breakpoints, spacing, mixinsInput),
      overrides: {},
      // Inject custom styles
      palette: palette,
      props: {},
      // Inject custom properties
      shadows: shadowsInput || shadows,
      typography: createTypography(palette, typographyInput),
      spacing: spacing,
    },
    cjs(
      {
        shape: shape,
        transitions: transitions,
        zIndex: zIndex,
      },
      other,
      {
        isMergeableObject: isPlainObject,
      },
    ),
  );
  return muiTheme;
}

var defaultTheme = createMuiTheme();

function makeStyles$1(stylesOrCreator) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return makeStyles(
    stylesOrCreator,
    _extends_1(
      {
        defaultTheme: defaultTheme,
      },
      options,
    ),
  );
}

function withStyles$1(stylesOrCreator, options) {
  return withStyles(
    stylesOrCreator,
    _extends_1(
      {
        defaultTheme: defaultTheme,
      },
      options,
    ),
  );
}

var useToolbarStyles = makeStyles$1(function(theme) {
  return {
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    spacer: {
      flex: '1 1 100%',
    },
    actions: {
      color: theme.palette.text.secondary,
      display: 'flex',
    },
  };
});
var useTableStyles = makeStyles$1(function(theme) {
  return {
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    card: {
      minWidth: 275,
    },
    table: {
      minWidth: 750,
    },
    tableWrapper: {
      overflowX: 'auto',
    },
    progress: {
      margin: theme.spacing(2),
      display: 'flex',
      justifyContent: 'center',
      minHeight: '120px',
      alignItems: 'center',
    },
  };
});

var DataTableToolbar = function DataTableToolbar(_ref) {
  var rows = _ref.rows,
    page = _ref.page,
    rowsPerPage = _ref.rowsPerPage,
    setPage = _ref.setPage,
    setRowsPerPage = _ref.setRowsPerPage;

  function handleChangePage(event, newPage) {
    setPage(newPage, rowsPerPage);
  }

  function handleChangeRowsPerPage(event) {
    setRowsPerPage(+event.target.value);
    setPage(0, +event.target.value);
  }

  var matches = core.useMediaQuery(core.useTheme().breakpoints.up('sm'));
  var classes = useToolbarStyles();
  return React__default.createElement(
    core.Toolbar,
    {
      className: classes.root,
    },
    React__default.createElement(
      core.Tooltip,
      {
        title: 'Delete',
      },
      React__default.createElement(
        core.IconButton,
        {
          'aria-label': 'Delete',
        },
        React__default.createElement(icons.Delete, null),
      ),
    ),
    matches &&
      React__default.createElement(
        core.Box,
        {
          style: {
            flex: 2,
          },
        },
        React__default.createElement(core.TablePagination, {
          rowsPerPageOptions: [5, 10, 25],
          component: 'div',
          count: rows.length,
          rowsPerPage: rowsPerPage,
          page: page,
          backIconButtonProps: {
            'aria-label': 'Previous Page',
          },
          nextIconButtonProps: {
            'aria-label': 'Next Page',
          },
          onChangePage: handleChangePage,
          onChangeRowsPerPage: handleChangeRowsPerPage,
        }),
      ),
  );
};

DataTableToolbar.propTypes = {
  rows: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
  page: PropTypes__default.number,
  rowsPerPage: PropTypes__default.number,
  setRowsPerPage: PropTypes__default.func.isRequired,
  setPage: PropTypes__default.func.isRequired,
};
DataTableToolbar.defaultProps = {
  page: 0,
  rowsPerPage: 10,
};

/* eslint-disable no-nested-ternary */

var DataTableColumn = function DataTableColumn(_ref) {
  var index = _ref.index,
    type = _ref.type,
    disablePadding = _ref.disablePadding,
    label = _ref.label,
    render = _ref.render;
  return React__default.createElement(React.Fragment, null);
};

DataTableColumn.propTypes = {
  index: PropTypes__default.string.isRequired,
  label: PropTypes__default.string.isRequired,
  render: PropTypes__default.func,
  disablePadding: PropTypes__default.bool,
  type: PropTypes__default.string,
};
DataTableColumn.defaultProps = {
  disablePadding: false,
  type: 'string',
  render: function render(identity) {
    return identity;
  },
};

var DataTableHeadView = function DataTableHeadView(props) {
  var order = props.order,
    orderBy = props.orderBy,
    onRequestSort = props.onRequestSort,
    cols = props.cols;

  var createSortHandler = function createSortHandler(property) {
    return function(event) {
      onRequestSort(event, property);
    };
  };

  return React__default.createElement(
    core.TableHead,
    null,
    React__default.createElement(
      core.TableRow,
      null,
      cols.map(function() {
        var col = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var index = col.index,
          type = col.type,
          disablePadding = col.disablePadding,
          label = col.label;
        return React__default.createElement(
          core.TableCell,
          {
            key: index,
            align:
              type.toLowerCase() === 'numeric'
                ? 'right'
                : type.toLowerCase() === 'string'
                ? 'left'
                : 'center',
            padding: disablePadding ? 'none' : 'default',
            sortDirection: orderBy === index ? order : false,
          },
          React__default.createElement(
            core.TableSortLabel,
            {
              active: orderBy === index,
              direction: order,
              onClick: createSortHandler(index),
            },
            label,
          ),
        );
      }),
    ),
  );
};

DataTableHeadView.propTypes = {
  onRequestSort: PropTypes__default.func.isRequired,
  order: PropTypes__default.string.isRequired,
  orderBy: PropTypes__default.string.isRequired,
  cols: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
};

var DataTableHead = function DataTableHead() {
  return React__default.createElement(React.Fragment, null);
};

DataTableHead.displayName = 'DataTableHead';

//
// A strict capitalization should uppercase the first letter of each word a the sentence.
// We only handle the first word.

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * @ignore - internal component.
 */

var TableContext = React__default.createContext();

/**
 * @ignore - internal component.
 */

var Tablelvl2Context = React__default.createContext();

var styles = {
  /* Styles applied to the root element. */
  root: {
    display: 'table-row-group',
  },
};
var tablelvl2 = {
  variant: 'body',
};
var TableBody = React__default.forwardRef(function TableBody(props, ref) {
  var classes = props.classes,
    className = props.className,
    _props$component = props.component,
    Component = _props$component === void 0 ? 'tbody' : _props$component,
    other = objectWithoutProperties(props, ['classes', 'className', 'component']);

  return React__default.createElement(
    Tablelvl2Context.Provider,
    {
      value: tablelvl2,
    },
    React__default.createElement(
      Component,
      _extends_1(
        {
          className: clsx(classes.root, className),
          ref: ref,
        },
        other,
      ),
    ),
  );
});
var TableBody$1 = withStyles$1(styles, {
  name: 'MuiTableBody',
})(TableBody);

var styles$1 = function styles(theme) {
  return {
    /* Styles applied to the root element. */
    root: _extends_1({}, theme.typography.body2, {
      display: 'table-cell',
      verticalAlign: 'inherit',
      // Workaround for a rendering bug with spanned columns in Chrome 62.0.
      // Removes the alpha (sets it to 1), and lightens or darkens the theme color.
      borderBottom: '1px solid\n    '.concat(
        theme.palette.type === 'light'
          ? lighten(fade(theme.palette.divider, 1), 0.88)
          : darken(fade(theme.palette.divider, 1), 0.68),
      ),
      textAlign: 'left',
      padding: '14px 40px 14px 16px',
      '&:last-child': {
        paddingRight: 16,
      },
    }),

    /* Styles applied to the root element if `variant="head"` or `context.table.head`. */
    head: {
      color: theme.palette.text.secondary,
      fontSize: theme.typography.pxToRem(12),
      lineHeight: theme.typography.pxToRem(21),
      fontWeight: theme.typography.fontWeightMedium,
    },

    /* Styles applied to the root element if `variant="body"` or `context.table.body`. */
    body: {
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightRegular,
    },

    /* Styles applied to the root element if `variant="footer"` or `context.table.footer`. */
    footer: {
      color: theme.palette.text.secondary,
      lineHeight: theme.typography.pxToRem(21),
      fontSize: theme.typography.pxToRem(12),
    },

    /* Styles applied to the root element if `size="small"`. */
    sizeSmall: {
      padding: '6px 24px 6px 16px',
      '&:last-child': {
        paddingRight: 16,
      },
      '&$paddingCheckbox': {
        width: 24,
        // prevent the checkbox column from growing
        padding: '0px 12px 0 16px',
        '&:last-child': {
          paddingLeft: 12,
          paddingRight: 16,
        },
        '& > *': {
          padding: 0,
        },
      },
    },

    /* Styles applied to the root element if `padding="checkbox"`. */
    paddingCheckbox: {
      width: 48,
      // prevent the checkbox column from growing
      padding: '0 0 0 4px',
      '&:last-child': {
        paddingLeft: 0,
        paddingRight: 4,
      },
    },

    /* Styles applied to the root element if `padding="none"`. */
    paddingNone: {
      padding: 0,
      '&:last-child': {
        padding: 0,
      },
    },

    /* Styles applied to the root element if `align="left"`. */
    alignLeft: {
      textAlign: 'left',
    },

    /* Styles applied to the root element if `align="center"`. */
    alignCenter: {
      textAlign: 'center',
    },

    /* Styles applied to the root element if `align="right"`. */
    alignRight: {
      textAlign: 'right',
      flexDirection: 'row-reverse',
    },

    /* Styles applied to the root element if `align="justify"`. */
    alignJustify: {
      textAlign: 'justify',
    },
  };
};
var TableCell = React__default.forwardRef(function TableCell(props, ref) {
  var _props$align = props.align,
    align = _props$align === void 0 ? 'inherit' : _props$align,
    classes = props.classes,
    className = props.className,
    component = props.component,
    paddingProp = props.padding,
    scopeProp = props.scope,
    sizeProp = props.size,
    sortDirection = props.sortDirection,
    variant = props.variant,
    other = objectWithoutProperties(props, [
      'align',
      'classes',
      'className',
      'component',
      'padding',
      'scope',
      'size',
      'sortDirection',
      'variant',
    ]);

  var table = React__default.useContext(TableContext);
  var tablelvl2 = React__default.useContext(Tablelvl2Context);
  var Component;

  if (component) {
    Component = component;
  } else {
    Component = tablelvl2 && tablelvl2.variant === 'head' ? 'th' : 'td';
  }

  var scope = scopeProp;

  if (!scope && tablelvl2 && tablelvl2.variant === 'head') {
    scope = 'col';
  }

  var padding = paddingProp || (table && table.padding ? table.padding : 'default');
  var size = sizeProp || (table && table.size ? table.size : 'medium');
  var ariaSort = null;

  if (sortDirection) {
    ariaSort = sortDirection === 'asc' ? 'ascending' : 'descending';
  }

  return React__default.createElement(
    Component,
    _extends_1(
      {
        ref: ref,
        className: clsx(
          classes.root,
          className,
          (variant ? variant === 'head' : tablelvl2 && tablelvl2.variant === 'head') &&
            classes.head,
          (variant ? variant === 'body' : tablelvl2 && tablelvl2.variant === 'body') &&
            classes.body,
          (variant ? variant === 'footer' : tablelvl2 && tablelvl2.variant === 'footer') &&
            classes.footer,
          align !== 'inherit' && classes['align'.concat(capitalize(align))],
          padding !== 'default' && classes['padding'.concat(capitalize(padding))],
          size !== 'medium' && classes['size'.concat(capitalize(size))],
        ),
        'aria-sort': ariaSort,
        scope: scope,
      },
      other,
    ),
  );
});
var TableCell$1 = withStyles$1(styles$1, {
  name: 'MuiTableCell',
})(TableCell);

var styles$2 = function styles(theme) {
  return {
    /* Styles applied to the root element. */
    root: {
      color: 'inherit',
      display: 'table-row',
      verticalAlign: 'middle',
      // We disable the focus ring for mouse, touch and keyboard users.
      outline: 'none',
      '&$selected': {
        backgroundColor:
          theme.palette.type === 'light'
            ? 'rgba(0, 0, 0, 0.04)' // grey[100]
            : 'rgba(255, 255, 255, 0.08)',
      },
      '&$hover:hover': {
        backgroundColor:
          theme.palette.type === 'light'
            ? 'rgba(0, 0, 0, 0.07)' // grey[200]
            : 'rgba(255, 255, 255, 0.14)',
      },
    },

    /* Pseudo-class applied to the root element if `selected={true}`. */
    selected: {},

    /* Pseudo-class applied to the root element if `hover={true}`. */
    hover: {},

    /* Styles applied to the root element if table variant="head". */
    head: {},

    /* Styles applied to the root element if table variant="footer". */
    footer: {},
  };
};
/**
 * Will automatically set dynamic row height
 * based on the material table element parent (head, body, etc).
 */

var TableRow = React__default.forwardRef(function TableRow(props, ref) {
  var classes = props.classes,
    className = props.className,
    _props$component = props.component,
    Component = _props$component === void 0 ? 'tr' : _props$component,
    _props$hover = props.hover,
    hover = _props$hover === void 0 ? false : _props$hover,
    _props$selected = props.selected,
    selected = _props$selected === void 0 ? false : _props$selected,
    other = objectWithoutProperties(props, [
      'classes',
      'className',
      'component',
      'hover',
      'selected',
    ]);

  var tablelvl2 = React__default.useContext(Tablelvl2Context);
  return React__default.createElement(
    Component,
    _extends_1(
      {
        ref: ref,
        className: clsx(
          classes.root,
          className,
          tablelvl2 && [
            tablelvl2.variant === 'head' && classes.head,
            tablelvl2.variant === 'footer' && classes.footer,
          ],
          hover && classes.hover,
          selected && classes.selected,
        ),
      },
      other,
    ),
  );
});
var TableRow$1 = withStyles$1(styles$2, {
  name: 'MuiTableRow',
})(TableRow);

/* eslint-disable no-nested-ternary */

function desc(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }

  return b[orderBy] > a[orderBy] ? 1 : 0;
}

function stableSort(array, cmp) {
  var stabilizedThis = array.map(function(el, index) {
    return [el, index];
  });
  stabilizedThis.sort(function(a, b) {
    var order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(function(el) {
    return el[0];
  });
}

var getSorting = function getSorting(order, orderBy) {
  return order === 'desc'
    ? function(a, b) {
        return desc(a, b, orderBy);
      }
    : function(a, b) {
        return -desc(a, b, orderBy);
      };
};

var DataTableBodyView = function DataTableBodyView(_ref) {
  var cols = _ref.cols,
    rows = _ref.rows,
    page = _ref.page,
    rowsPerPage = _ref.rowsPerPage,
    order = _ref.order,
    orderBy = _ref.orderBy;
  var emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  var sortedRows = stableSort(rows, getSorting(order, orderBy)).slice(
    page * rowsPerPage,
    page * rowsPerPage + rowsPerPage,
  );
  return React__default.createElement(
    TableBody$1,
    null,
    sortedRows.map(function(row) {
      return React__default.createElement(
        TableRow$1,
        {
          hover: true,
          tabIndex: -1,
          key: row.id,
        },
        cols.map(function(_ref2) {
          var index = _ref2.index,
            type = _ref2.type,
            _ref2$render = _ref2.render,
            render =
              _ref2$render === void 0
                ? function(el) {
                    return el;
                  }
                : _ref2$render;
          return React__default.createElement(
            TableCell$1,
            {
              key: index,
              align:
                type.toLowerCase() === 'numeric'
                  ? 'right'
                  : type.toLowerCase() === 'string'
                  ? 'left'
                  : 'center',
            },
            render(row[index]),
          );
        }),
      );
    }),
    emptyRows > 0 &&
      React__default.createElement(
        TableRow$1,
        {
          style: {
            height: 49 * emptyRows,
          },
        },
        React__default.createElement(TableCell$1, {
          colSpan: 6,
        }),
      ),
  );
};

DataTableBodyView.propTypes = {
  cols: PropTypes__default.arrayOf(
    PropTypes__default.shape({
      index: '',
      render: '',
    }),
  ).isRequired,
  rows: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
  page: PropTypes__default.number.isRequired,
  rowsPerPage: PropTypes__default.number.isRequired,
  order: PropTypes__default.string,
  orderBy: PropTypes__default.string,
};
DataTableBodyView.defaultProps = {
  order: 'asc',
  orderBy: '',
};

var DataTableBody = function DataTableBody() {
  return React__default.createElement(React.Fragment, null);
};

DataTableBody.displayName = 'DataTableBody';

function DataTableView(props) {
  var classes = useTableStyles();
  var rows = props.rows,
    page = props.page,
    cols = props.cols,
    isTableBody = props.isTableBody,
    setPage = props.setPage,
    setRowsPerPage = props.setRowsPerPage,
    rowsPerPage = props.rowsPerPage,
    order = props.order,
    setOrder = props.setOrder,
    orderBy = props.orderBy,
    setOrderBy = props.setOrderBy;

  function handleRequestSort(event, property) {
    var isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  }

  return React__default.createElement(
    'div',
    {
      className: classes.root,
    },
    React__default.createElement(
      core.Paper,
      {
        className: classes.paper,
      },
      React__default.createElement(DataTableToolbar, {
        rows: rows,
        page: page,
        rowsPerPage: rowsPerPage,
        setPage: setPage,
        setRowsPerPage: setRowsPerPage,
      }),
      React__default.createElement(
        'div',
        {
          className: classes.tableWrapper,
        },
        React__default.createElement(
          core.Table,
          {
            className: classes.table,
            'aria-labelledby': 'tableTitle',
            size: 'medium',
          },
          React__default.createElement(DataTableHeadView, {
            cols: cols,
            order: order,
            orderBy: orderBy,
            onRequestSort: handleRequestSort,
            rowCount: rows.length,
          }),
          rows.length > 0 &&
            isTableBody &&
            React__default.createElement(DataTableBodyView, {
              rows: rows,
              cols: cols,
              page: page,
              rowsPerPage: rowsPerPage,
              order: order,
              orderBy: orderBy,
            }),
        ),
        rows.length === 0 &&
          React__default.createElement(
            'div',
            {
              className: classes.progress,
            },
            React__default.createElement(core.CircularProgress, null),
          ),
      ),
    ),
  );
}
DataTableView.propTypes = {
  rows: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
  page: PropTypes__default.number,
  cols: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
  isTableBody: PropTypes__default.bool,
  order: PropTypes__default.oneOf(['desc', 'asc']),
  orderBy: PropTypes__default.string,
  rowsPerPage: PropTypes__default.number,
  setRowsPerPage: PropTypes__default.func.isRequired,
  setPage: PropTypes__default.func.isRequired,
  setOrder: PropTypes__default.func.isRequired,
  setOrderBy: PropTypes__default.func.isRequired,
};
DataTableView.defaultProps = {
  page: 0,
  isTableBody: true,
  order: 'asc',
  orderBy: '',
  rowsPerPage: 10,
};

function DataTableCard(props) {
  var classes = useTableStyles();
  var rows = props.rows,
    page = props.page,
    cols = props.cols,
    setPage = props.setPage,
    rowsPerPage = props.rowsPerPage,
    setRowsPerPage = props.setRowsPerPage;

  function handleNext() {
    setPage(page + 1);
  }

  function handleBack() {
    setPage(page - 1);
  }

  var steps = rows.length ? Math.max(Math.round(+rows.length / rowsPerPage), 1) : 1;
  return React__default.createElement(
    'div',
    {
      className: classes.root,
    },
    React__default.createElement(
      core.Paper,
      {
        className: classes.paper,
      },
      React__default.createElement(DataTableToolbar, {
        rows: rows,
        page: page,
        rowsPerPage: rowsPerPage,
        setPage: setPage,
        setRowsPerPage: setRowsPerPage,
      }),
      rows.map(function(row, idx) {
        return React__default.createElement(
          core.Card,
          {
            className: classes.card,
            key: row.id || idx,
          },
          React__default.createElement(
            core.CardContent,
            null,
            cols.map(function(_ref) {
              var index = _ref.index,
                label = _ref.label,
                _ref$render = _ref.render,
                render =
                  _ref$render === void 0
                    ? function(el) {
                        return el;
                      }
                    : _ref$render;
              return React__default.createElement(
                'div',
                {
                  key: index,
                },
                React__default.createElement(
                  core.Typography,
                  {
                    variant: 'subtitle1',
                  },
                  label,
                ),
                React__default.createElement(
                  core.Typography,
                  {
                    variant: 'subtitle2',
                    color: 'textSecondary',
                    gutterBottom: true,
                  },
                  render(row[index]),
                ),
              );
            }),
          ),
        );
      }),
      rows.length === 0 &&
        React__default.createElement(
          'div',
          {
            className: classes.progress,
          },
          React__default.createElement(core.CircularProgress, null),
        ),
      React__default.createElement(core.MobileStepper, {
        variant: 'dots',
        steps: steps,
        position: 'static',
        activeStep: page,
        className: classes.root,
        nextButton: React__default.createElement(
          core.Button,
          {
            size: 'small',
            onClick: handleNext,
            disabled: rows.length < rowsPerPage,
          },
          'Next',
          React__default.createElement(icons.KeyboardArrowRight, null),
        ),
        backButton: React__default.createElement(
          core.Button,
          {
            size: 'small',
            onClick: handleBack,
            disabled: page === 0,
          },
          React__default.createElement(icons.KeyboardArrowLeft, null),
          'Back',
        ),
      }),
    ),
  );
}
DataTableCard.propTypes = {
  rows: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
  page: PropTypes__default.number,
  cols: PropTypes__default.arrayOf(PropTypes__default.object).isRequired,
  setRowsPerPage: PropTypes__default.func.isRequired,
  rowsPerPage: PropTypes__default.number,
  setPage: PropTypes__default.func.isRequired,
};
DataTableCard.defaultProps = {
  page: 0,
  rowsPerPage: 10,
};

var DataTable = withStyles({
  root: {},
})(function(props) {
  var matches = core.useMediaQuery(core.useTheme().breakpoints.up('sm'));
  var withProps = DataTableContainer(props);
  return React__default.createElement(
    StylesProvider,
    {
      injectFirst: true,
    },
    matches
      ? React__default.createElement(DataTableView, _extends({}, withProps, props))
      : React__default.createElement(DataTableCard, _extends({}, withProps, props)),
  );
});

exports.DataTable = DataTable;
exports.DataTableBody = DataTableBody;
exports.DataTableColumn = DataTableColumn;
exports.DataTableHead = DataTableHead;
