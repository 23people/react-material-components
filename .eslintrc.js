module.exports = {
  env: {
    browser: true,
    jest: true,
    es6: true,
    'cypress/globals': true,
  },
  extends: ['airbnb', 'react-app', 'prettier', 'prettier/react'],
  parser: 'babel-eslint',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react', 'prettier', 'cypress'],
  rules: {
    'import/prefer-default-export': ['off'],
    'prefer-promise-reject-errors': ['off'],
    'react/jsx-filename-extension': ['off'],
    'react/prop-types': ['warn'],
    'no-return-assign': ['off'],
  },
};
