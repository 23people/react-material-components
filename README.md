# material-react-storybook

## Features

- React 16
- Webpack 4
- Babel
- Staging ES Next Features
- Hot Module Replacement
- Testing
  - Jest
  - Integration Test
  - Snapshot Test (Jest)
  - Visual Test
  - Travis CI
  - Coveralls.io
- Storybook 4
  - Knobs
  - Actions
  - Viewport
  - Source

## Installation

- `git clone <repo>`
- `cd <folder>`
- npm install -g commitizen
- npm install
- npm start
- visit `http://localhost:8080/`

## Running Storybook

- npm run start

## Running Unit Tests

- npm run test:unit

## Running E2E Tests

- npm run test:e2e

## Running Visual Regression Tests

- docker-compose up -d --build
- docker-compose run cypress ./node_modules/.bin/cypress run --env

Updating snapshots
Run Cypress with --env updateSnapshots=true in order to update the base image files for all of your tests.

Preventing failures
Run Cypress with --env failOnSnapshotDiff=false in order to prevent test failures when an image diff does not pass.
