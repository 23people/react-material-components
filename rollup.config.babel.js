import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';

import { name, version } from './package.json';

const output = {
  name,
  file: `./lib/${name}-${version}.js`,
  format: 'umd',
  sourcemap: true,
  globals: {
    react: 'React',
    'react-dom': 'ReactDOM',
    'prop-types': 'PropTypes',
    '@material-ui/core': 'MaterialUICore',
    '@material-ui/icons': 'MaterialUIIcons',
  },
};
export default {
  input: './src/index.js',
  output: [
    output,
    {
      file: 'dist/index.js',
      format: 'cjs',
      exports: 'named',
    },
  ],
  plugins: [
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    babel({
      exclude: 'node_modules/**',
      presets: [
        [
          '@babel/preset-env',
          {
            targets: {
              browsers: ['last 2 versions', 'ie > 11'],
            },
            debug: false,
            modules: false,
          },
        ],
        '@babel/preset-react',
      ],
      plugins: [
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-react-jsx',
      ],
      babelrc: false,
    }),
    resolve(),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/react-is/index.js': ['isElement', 'isValidElementType', 'ForwardRef'],
      },
    }),
  ],
  external: ['react', 'react-dom', 'prop-types', '@material-ui/core', '@material-ui/icons'],
};
